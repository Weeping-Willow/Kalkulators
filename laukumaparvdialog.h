#ifndef LAUKUMAPARVDIALOG_H
#define LAUKUMAPARVDIALOG_H

#include <QDialog>

namespace Ui {
class LaukumaParvDialog;
}

class LaukumaParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LaukumaParvDialog(QWidget *parent = nullptr);
    ~LaukumaParvDialog();

private slots:


    void on_LaukumaParvPushButton_clicked();

private:
    Ui::LaukumaParvDialog *ui;
};

#endif // LAUKUMAPARVDIALOG_H

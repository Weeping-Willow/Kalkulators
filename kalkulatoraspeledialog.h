#ifndef KALKULATORASPELEDIALOG_H
#define KALKULATORASPELEDIALOG_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class KalkulatoraSpeleDialog;
}

class KalkulatoraSpeleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KalkulatoraSpeleDialog(QWidget *parent = nullptr);
    ~KalkulatoraSpeleDialog();
    QTimer *timer;


private slots:


    void on_buttonSpelet_clicked();

    void on_buttonEnd_clicked();

    void updateTime();

    void on_checkIfCorrect_clicked();

    void on_pushButton_clicked();

private:
    Ui::KalkulatoraSpeleDialog *ui;
};

#endif // KALKULATORASPELEDIALOG_H

#-------------------------------------------------
#
# Project created by QtCreator 2019-05-01T10:24:18
#
#-------------------------------------------------

QT       += core gui network  sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Eksamena_Projekts_Prog_Kalkulators
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11


SOURCES += \
        atrumaparvdialog.cpp \
        datumaparvdialog.cpp \
        datuparvdialog.cpp \
        garumaparvdialog.cpp \
        instructiondialog.cpp \
        kalkulatoraspeledialog.cpp \
        kalkulatorsdialog.cpp \
        laikaparvdialog.cpp \
        laukumaparvdialog.cpp \
        main.cpp \
        mainwindow.cpp \
        masasparvdialog.cpp \
        randomnumber.cpp \
        tilpumaparvdialog.cpp \
        valutasparvdialog.cpp

HEADERS += \
        atrumaparvdialog.h \
        datumaparvdialog.h \
        datuparvdialog.h \
        garumaparvdialog.h \
        instructiondialog.h \
        kalkulatoraspeledialog.h \
        kalkulatorsdialog.h \
        laikaparvdialog.h \
        laukumaparvdialog.h \
        mainwindow.h \
        masasparvdialog.h \
        randomnumber.h \
        tilpumaparvdialog.h \
        valutasparvdialog.h

FORMS += \
        atrumaparvdialog.ui \
        datumaparvdialog.ui \
        datuparvdialog.ui \
        garumaparvdialog.ui \
        instructiondialog.ui \
        kalkulatoraspeledialog.ui \
        kalkulatorsdialog.ui \
        laikaparvdialog.ui \
        laukumaparvdialog.ui \
        mainwindow.ui \
        masasparvdialog.ui \
        speledialog.ui \
        tilpumaparvdialog.ui \
        valutasparvdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Currency.json \
    valuesCurrency.db

#include "kalkulatoraspeledialog.h"
#include "ui_kalkulatoraspeledialog.h"
#include <QTimer>
#include <QtDebug>
#include <QRandomGenerator>
#include "randomnumber.h"
#include <cmath>
#include <instructiondialog.h>

int scoreOutput = 0,range1,range2,decimalNumber;
float resultForEquation,value1,value2;
QString actionType;

KalkulatoraSpeleDialog::KalkulatoraSpeleDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KalkulatoraSpeleDialog)
{
    ui->setupUi(this);

    QStringList names= {"Saskaitīšana","Atņemšana","Reizināšana","Dalīnašana"};
    foreach (QString item, names)
    {
        ui->comboBoxType->addItem(item);
    }

    QStringList names1= {"Viegli","Videji","Grūti"};
    foreach (QString item, names1)
    {
        ui->comboBoxGrutiba->addItem(item);
    }

    QStringList names2= {"60","90","120"};
    foreach (QString item, names2)
    {
        ui->comboBoxTime->addItem(item);
    }
    ui->Input->hide();
    ui->Output->hide();
    ui->label_2->hide();
    ui->label_3->hide();
    ui->buttonEnd->hide();
    ui->checkIfCorrect->hide();
    ui->time->hide();
    ui->score->hide();



}

KalkulatoraSpeleDialog::~KalkulatoraSpeleDialog()
{
    delete ui;
}



void KalkulatoraSpeleDialog::on_buttonSpelet_clicked()
{

    ui->comboBoxTime->hide();
    ui->comboBoxType->hide();
    ui->comboBoxGrutiba->hide();
    ui->buttonSpelet->hide();



    ui->Input->show();
    ui->Output->show();
    ui->label_2->show();
    ui->label_3->show();
    ui->buttonEnd->show();
    ui->checkIfCorrect->show();
    ui->time->show();
    ui->score->show();

    QString temp1 = ui->comboBoxType ->currentText();
    QString temp2 = ui->comboBoxGrutiba->currentText();
    QString temp3 = ui->comboBoxTime ->currentText();
    double tempTime = temp3.toDouble();

    range1,range2;
    int gameTime = temp3.toDouble();


    if(temp1 == "Saskaitīšana")
    {
        actionType = "+";
        if (temp2 =="Viegli")
        {
            range1 = 0;
            range2 = 100;
            decimalNumber = 0;

        }
         else if (temp2 =="Videji")
        {
            range1 = 0;
            range2 = 500;
            decimalNumber = 1;
        }
        else if (temp2 =="Grūti")
        {
            range1 = 0;
            range2 = 1000;
            decimalNumber = 2;
        }
    }

    else if(temp1 == "Atņemšana")
    {
        actionType = "-";
        if (temp2 =="Viegli")
        {
            range1 = 0;
            range2 = 100;
            decimalNumber = 0;
        }
        else if (temp2 =="Videji")
        {
            range1 = 0;
            range2 = 500;
            decimalNumber = 1;
        }
        else if (temp2 =="Grūti")
        {
            range1 = 0;
            range2 = 1000;
            decimalNumber = 2;
        }
    }

    else if(temp1 == "Reizināšana")
    {
        actionType = "*";
        if (temp2 =="Viegli")
        {
            qDebug()<<"sda";
            range1 = 0;
            range2 = 10;
            decimalNumber = 0;
        }
        if (temp2 =="Videji")
        {
            range1 = 0;
            range2 = 20;
            decimalNumber = 1;
        }
        if (temp2 =="Grūti")
        {
            range1 = 0;
            range2 = 50;
            decimalNumber = 2;
        }
    }

    else if(temp1 == "Dalīnašana")
    {
        actionType = "/";
        if (temp2 =="Viegli")
        {
            range1 = 1;
            range2 = 25;
            decimalNumber = 0;
        }
        if (temp2 =="Videji")
        {
            range1 = 1;
            range2 = 100;
            decimalNumber = 1;
        }
        if (temp2 =="Grūti")
        {
            range1 = 1;
            range2 = 500;
            decimalNumber = 2;
        }
    }


    quint32 valueRandSeed = QRandomGenerator::global()->generate();
   value1  = randomNumberGen(range1,range2,valueRandSeed,decimalNumber);
   qDebug()<<"wel this is awsome"<<value1;

   valueRandSeed = QRandomGenerator::global()->generate();
   value2 = randomNumberGen(range1,range2,valueRandSeed,decimalNumber);



   QString outputValue1 =QString::number(value1,'g',6);
   QString outputValue2 =QString::number(value2,'g',6);

   QString outputString = outputValue1 + actionType +outputValue2;

   ui->Output->setText(outputString);

   timer = new QTimer(this);
   connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));

   ui->time->display(temp3);
   timer->start(1000);
}

void KalkulatoraSpeleDialog::on_buttonEnd_clicked()
{
    ui->Input->hide();
    ui->Output->hide();
    ui->label_2->hide();
    ui->label_3->hide();
    ui->buttonEnd->hide();
    ui->checkIfCorrect->hide();
    ui->time->hide();
    ui->score->hide();

    ui->comboBoxTime->show();
    ui->comboBoxType->show();
    ui->comboBoxGrutiba->show();
    ui->buttonSpelet->show();

    timer->stop();
    timer->disconnect();
    scoreOutput = 0;
    ui->score->display(scoreOutput);

}

void KalkulatoraSpeleDialog::updateTime()
{

    double tempTime = ui->time->value();

    if (tempTime == 0)
       timer->stop();
    else
    {
        tempTime = tempTime - 1;
        ui->time->display(tempTime);
    }

}

void KalkulatoraSpeleDialog::on_checkIfCorrect_clicked()
{
     double tempTime = ui->time->value();
    if (tempTime == 0);

    else
    {
        if (actionType == "+")
            resultForEquation = value1+value2;
        else if (actionType == "-")
            resultForEquation = value1 - value2;
        else if(actionType == "*")
            resultForEquation = value1 * value2;
        else if (actionType == "/")
            resultForEquation = round(value1/value2);

        float userInput = ui->Input->text().toFloat();
        qDebug()<<resultForEquation;
        qDebug()<<userInput;
        if(userInput == resultForEquation)
        {
            scoreOutput++;
            ui->score->display(scoreOutput);

            quint32 valueRandSeed = QRandomGenerator::global()->generate();
            value1  = randomNumberGen(range1,range2,valueRandSeed,decimalNumber);

            valueRandSeed = QRandomGenerator::global()->generate();
            value2 = randomNumberGen(range1,range2,valueRandSeed,decimalNumber);

            QString outputValue1 =QString::number(value1,'g',6);
            QString outputValue2 =QString::number(value2,'g',6);

            QString outputString = outputValue1 + actionType +outputValue2;

            ui->Output->setText(outputString);

        }
        else
        {
            scoreOutput--;
            ui->score->display(scoreOutput);
        }
   }
}


void KalkulatoraSpeleDialog::on_pushButton_clicked()
{

        InstructionDialog DatuParv;
        DatuParv.setModal(true);
        DatuParv.exec();

}

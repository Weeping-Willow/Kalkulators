#include "atrumaparvdialog.h"
#include "ui_atrumaparvdialog.h"

AtrumaParvDialog::AtrumaParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AtrumaParvDialog)
{
    ui->setupUi(this);




    QStringList Names= {"Jūdzas","Metri","Pēdas","Kilometri","Mezgli"};
    foreach (QString item, Names)
    {
        ui->comboBoxAtrumaParv->addItem(item);
        ui->comboBoxAtrumaParv2->addItem(item);
    }
    //pievieno comboBox šos nosaukumus
}

AtrumaParvDialog::~AtrumaParvDialog()
{
    delete ui;
}



void AtrumaParvDialog::on_pushButton_clicked()
{
    const double judzas= 0.6213711922;
    const double metri =1000;
    const double pedas =3280.839895;
    const double kilometri=1;
    const double mezgli = 0.5399568035;

    double value;

    QString temp1,temp2;
    temp1 =ui->comboBoxAtrumaParv->currentText();
    temp2 =ui->comboBoxAtrumaParv2->currentText();
    value = ui->AtrumaInput->value();

    if (temp1 == "Jūdzas")
        value = value /judzas;

    else if (temp1 == "Metri")
    {
        value = value/metri;
    }

    else if (temp1 == "Pēdas")
    {
        value = value/pedas;
    }

    else if (temp1 == "Kilometri")
    {
         value = value/kilometri;
    }

    else if (temp1 == "Mezgli")
    {
        value = value/mezgli;
    }



    if (temp2 == "Jūdzas")
        value = value*judzas;

    else if (temp2 == "Metri")
    {
        value = value*metri;
    }

    else if (temp2 == "Pēdas")
    {
        value = value*pedas;
    }

    else if (temp2 == "Kilometri")
    {
         value = value*kilometri;
    }

    else if (temp2 == "Mezgli")
    {
        value = value*mezgli;
    }

    std::string OutPut = std::to_string(value);
    QString RealOutPut = QString::fromStdString(OutPut);
    ui->AtrumaOutput->setText(RealOutPut);
}

/********************************************************************************
** Form generated from reading UI file 'instructiondialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INSTRUCTIONDIALOG_H
#define UI_INSTRUCTIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_InstructionDialog
{
public:
    QPushButton *pushButton;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QDialog *InstructionDialog)
    {
        if (InstructionDialog->objectName().isEmpty())
            InstructionDialog->setObjectName(QString::fromUtf8("InstructionDialog"));
        InstructionDialog->resize(400, 300);
        pushButton = new QPushButton(InstructionDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(140, 210, 88, 34));
        label = new QLabel(InstructionDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 30, 211, 18));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font.setPointSize(13);
        label->setFont(font);
        label_2 = new QLabel(InstructionDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 70, 351, 18));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font1.setPointSize(13);
        font1.setBold(false);
        font1.setWeight(50);
        label_2->setFont(font1);
        label_3 = new QLabel(InstructionDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 100, 391, 51));
        label_3->setFont(font);

        retranslateUi(InstructionDialog);
        QObject::connect(pushButton, SIGNAL(clicked()), InstructionDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(InstructionDialog);
    } // setupUi

    void retranslateUi(QDialog *InstructionDialog)
    {
        InstructionDialog->setWindowTitle(QApplication::translate("InstructionDialog", "Dialog", nullptr));
        pushButton->setText(QApplication::translate("InstructionDialog", "Aizvert", nullptr));
        label->setText(QApplication::translate("InstructionDialog", "Viegli - Skait\304\274i ir vesali", nullptr));
        label_2->setText(QApplication::translate("InstructionDialog", "Vid\304\223ji  -Skait\304\274im ir viens decim\304\201l cipars aiz komata", nullptr));
        label_3->setText(QApplication::translate("InstructionDialog", "Gr\305\253ti - Skat\304\274iem ir divi decim\304\201l cipari aiz komata", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InstructionDialog: public Ui_InstructionDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INSTRUCTIONDIALOG_H

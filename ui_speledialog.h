/********************************************************************************
** Form generated from reading UI file 'speledialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPELEDIALOG_H
#define UI_SPELEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_SpeleDialog
{
public:
    QLCDNumber *score;
    QLCDNumber *time;
    QLabel *Output;
    QLabel *label_2;
    QLabel *label_3;
    QPushButton *buttonEnd;
    QPushButton *checkIfCorrect;
    QDoubleSpinBox *Input;

    void setupUi(QDialog *SpeleDialog)
    {
        if (SpeleDialog->objectName().isEmpty())
            SpeleDialog->setObjectName(QString::fromUtf8("SpeleDialog"));
        SpeleDialog->resize(392, 357);
        score = new QLCDNumber(SpeleDialog);
        score->setObjectName(QString::fromUtf8("score"));
        score->setGeometry(QRect(220, 30, 171, 101));
        score->setLayoutDirection(Qt::LeftToRight);
        score->setFrameShadow(QFrame::Plain);
        score->setLineWidth(2);
        score->setMidLineWidth(-2);
        time = new QLCDNumber(SpeleDialog);
        time->setObjectName(QString::fromUtf8("time"));
        time->setGeometry(QRect(0, 30, 171, 101));
        time->setStyleSheet(QString::fromUtf8(" qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
""));
        time->setFrameShadow(QFrame::Plain);
        time->setLineWidth(2);
        Output = new QLabel(SpeleDialog);
        Output->setObjectName(QString::fromUtf8("Output"));
        Output->setGeometry(QRect(0, 130, 391, 71));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font.setPointSize(20);
        Output->setFont(font);
        Output->setStyleSheet(QString::fromUtf8(" qproperty-alignment: 'AlignVCenter | AlignRight';\n"
"  border: 1px solid gray;"));
        label_2 = new QLabel(SpeleDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(0, 10, 171, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font1.setPointSize(18);
        label_2->setFont(font1);
        label_3 = new QLabel(SpeleDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(220, 10, 171, 20));
        label_3->setFont(font1);
        buttonEnd = new QPushButton(SpeleDialog);
        buttonEnd->setObjectName(QString::fromUtf8("buttonEnd"));
        buttonEnd->setGeometry(QRect(330, 0, 61, 31));
        checkIfCorrect = new QPushButton(SpeleDialog);
        checkIfCorrect->setObjectName(QString::fromUtf8("checkIfCorrect"));
        checkIfCorrect->setGeometry(QRect(130, 300, 131, 51));
        Input = new QDoubleSpinBox(SpeleDialog);
        Input->setObjectName(QString::fromUtf8("Input"));
        Input->setGeometry(QRect(0, 200, 391, 71));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font2.setPointSize(25);
        Input->setFont(font2);
        Input->setLayoutDirection(Qt::LeftToRight);
        Input->setDecimals(0);
        Input->setMinimum(-625.000000000000000);
        Input->setMaximum(625.000000000000000);

        retranslateUi(SpeleDialog);

        QMetaObject::connectSlotsByName(SpeleDialog);
    } // setupUi

    void retranslateUi(QDialog *SpeleDialog)
    {
        SpeleDialog->setWindowTitle(QApplication::translate("SpeleDialog", "Dialog", nullptr));
        Output->setText(QString());
        label_2->setText(QApplication::translate("SpeleDialog", "Laiks", nullptr));
        label_3->setText(QApplication::translate("SpeleDialog", "Rezultats", nullptr));
        buttonEnd->setText(QApplication::translate("SpeleDialog", "Beigt", nullptr));
        checkIfCorrect->setText(QApplication::translate("SpeleDialog", "Parbaudit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SpeleDialog: public Ui_SpeleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPELEDIALOG_H

/********************************************************************************
** Form generated from reading UI file 'atrumaparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ATRUMAPARVDIALOG_H
#define UI_ATRUMAPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AtrumaParvDialog
{
public:
    QLabel *label;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QDoubleSpinBox *AtrumaInput;
    QComboBox *comboBoxAtrumaParv;
    QHBoxLayout *horizontalLayout;
    QLineEdit *AtrumaOutput;
    QComboBox *comboBoxAtrumaParv2;
    QPushButton *pushButton;

    void setupUi(QDialog *AtrumaParvDialog)
    {
        if (AtrumaParvDialog->objectName().isEmpty())
            AtrumaParvDialog->setObjectName(QString::fromUtf8("AtrumaParvDialog"));
        AtrumaParvDialog->resize(400, 300);
        label = new QLabel(AtrumaParvDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(50, 20, 391, 21));
        layoutWidget = new QWidget(AtrumaParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(50, 60, 291, 171));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        AtrumaInput = new QDoubleSpinBox(layoutWidget);
        AtrumaInput->setObjectName(QString::fromUtf8("AtrumaInput"));
        AtrumaInput->setMaximum(99999999999.000000000000000);

        horizontalLayout_2->addWidget(AtrumaInput);

        comboBoxAtrumaParv = new QComboBox(layoutWidget);
        comboBoxAtrumaParv->setObjectName(QString::fromUtf8("comboBoxAtrumaParv"));

        horizontalLayout_2->addWidget(comboBoxAtrumaParv);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        AtrumaOutput = new QLineEdit(layoutWidget);
        AtrumaOutput->setObjectName(QString::fromUtf8("AtrumaOutput"));

        horizontalLayout->addWidget(AtrumaOutput);

        comboBoxAtrumaParv2 = new QComboBox(layoutWidget);
        comboBoxAtrumaParv2->setObjectName(QString::fromUtf8("comboBoxAtrumaParv2"));

        horizontalLayout->addWidget(comboBoxAtrumaParv2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_2->addWidget(pushButton);


        retranslateUi(AtrumaParvDialog);

        QMetaObject::connectSlotsByName(AtrumaParvDialog);
    } // setupUi

    void retranslateUi(QDialog *AtrumaParvDialog)
    {
        AtrumaParvDialog->setWindowTitle(QApplication::translate("AtrumaParvDialog", "Dialog", nullptr));
        label->setText(QApplication::translate("AtrumaParvDialog", "Merijumi tiek at\304\223loti laiak form\304\201ta h!!!", nullptr));
        pushButton->setText(QApplication::translate("AtrumaParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AtrumaParvDialog: public Ui_AtrumaParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ATRUMAPARVDIALOG_H

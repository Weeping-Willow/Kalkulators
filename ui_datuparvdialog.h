/********************************************************************************
** Form generated from reading UI file 'datuparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATUPARVDIALOG_H
#define UI_DATUPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DatuParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *DatuInput;
    QComboBox *comboBoxDati1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *DatuOutput;
    QComboBox *comboBoxDati2;
    QPushButton *DatuParvPushButton;

    void setupUi(QDialog *DatuParvDialog)
    {
        if (DatuParvDialog->objectName().isEmpty())
            DatuParvDialog->setObjectName(QString::fromUtf8("DatuParvDialog"));
        DatuParvDialog->resize(400, 300);
        layoutWidget = new QWidget(DatuParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(40, 40, 331, 211));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        DatuInput = new QDoubleSpinBox(layoutWidget);
        DatuInput->setObjectName(QString::fromUtf8("DatuInput"));
        DatuInput->setDecimals(6);
        DatuInput->setMaximum(9999999999.999998092651367);

        horizontalLayout_3->addWidget(DatuInput);

        comboBoxDati1 = new QComboBox(layoutWidget);
        comboBoxDati1->setObjectName(QString::fromUtf8("comboBoxDati1"));

        horizontalLayout_3->addWidget(comboBoxDati1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        DatuOutput = new QLineEdit(layoutWidget);
        DatuOutput->setObjectName(QString::fromUtf8("DatuOutput"));

        horizontalLayout_4->addWidget(DatuOutput);

        comboBoxDati2 = new QComboBox(layoutWidget);
        comboBoxDati2->setObjectName(QString::fromUtf8("comboBoxDati2"));

        horizontalLayout_4->addWidget(comboBoxDati2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        DatuParvPushButton = new QPushButton(layoutWidget);
        DatuParvPushButton->setObjectName(QString::fromUtf8("DatuParvPushButton"));

        verticalLayout->addWidget(DatuParvPushButton);


        retranslateUi(DatuParvDialog);

        QMetaObject::connectSlotsByName(DatuParvDialog);
    } // setupUi

    void retranslateUi(QDialog *DatuParvDialog)
    {
        DatuParvDialog->setWindowTitle(QApplication::translate("DatuParvDialog", "Dialog", nullptr));
        DatuParvPushButton->setText(QApplication::translate("DatuParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DatuParvDialog: public Ui_DatuParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATUPARVDIALOG_H

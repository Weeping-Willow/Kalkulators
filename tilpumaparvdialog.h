#ifndef TILPUMAPARVDIALOG_H
#define TILPUMAPARVDIALOG_H

#include <QDialog>

namespace Ui {
class TilpumaParvDialog;
}

class TilpumaParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TilpumaParvDialog(QWidget *parent = nullptr);
    ~TilpumaParvDialog();

private slots:
    void on_TilpumaParvPushButton_clicked();

private:
    Ui::TilpumaParvDialog *ui;
};

#endif // TILPUMAPARVDIALOG_H

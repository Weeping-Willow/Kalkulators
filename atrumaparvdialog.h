#ifndef ATRUMAPARVDIALOG_H
#define ATRUMAPARVDIALOG_H

#include <QDialog>

namespace Ui {
class AtrumaParvDialog;
}

class AtrumaParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AtrumaParvDialog(QWidget *parent = nullptr);
    ~AtrumaParvDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::AtrumaParvDialog *ui;
};

#endif // ATRUMAPARVDIALOG_H

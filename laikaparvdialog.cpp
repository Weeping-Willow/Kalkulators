#include "laikaparvdialog.h"
#include "ui_laikaparvdialog.h"

TempParvDialog::TempParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TempParvDialog)
{
    ui->setupUi(this);
    QStringList Names= {"Kelvini","Celsījs","Farenheita"};
    foreach (QString item, Names)
    {
        ui->comboBoxTemp1->addItem(item);
        ui->comboBoxTemp2->addItem(item);
    }
}

TempParvDialog::~TempParvDialog()
{
    delete ui;
}

void TempParvDialog::on_TempParvPushButton_clicked()
{

    double value;
    QString temp1,temp2;

    temp1 =ui->comboBoxTemp1->currentText();
    temp2 =ui->comboBoxTemp2->currentText();
    value = ui->TempInput->value();
    if (temp1 == "Celsījs"&& temp2 == "Farenheita")
        value = (value*1.8)+32;

    else if (temp2 == "Celsījs"&& temp1 == "Farenheita")
    {
        value = (value-32)*0.5556;
    }

    else if (temp1 == "Celsījs"&& temp2 == "Kelvini")
    {
        value = value+273.15;
    }

    else if (temp2 == "Celsijs"&& temp1 == "Kelvini")
    {
        value = value-273.15;
    }
    else if (temp1 == "Farenheita"&& temp2 == "Kelvini")
    {
        value = (value +459.67)*5/9;
    }
    else if (temp2 == "Farenheita"&& temp1 == "Kelvini")
    {
        value = value*9/5-459.67;
    }



   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->TempOutput->setText(RealOutPut);
}

#ifndef DATUPARVDIALOG_H
#define DATUPARVDIALOG_H

#include <QDialog>

namespace Ui {
class DatuParvDialog;
}

class DatuParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DatuParvDialog(QWidget *parent = nullptr);
    ~DatuParvDialog();

private slots:
    void on_DatuParvPushButton_clicked();

    void on_DatuInput_valueChanged(double arg1);

private:
    Ui::DatuParvDialog *ui;
};

#endif // DATUPARVDIALOG_H

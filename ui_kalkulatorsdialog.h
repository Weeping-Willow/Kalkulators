/********************************************************************************
** Form generated from reading UI file 'kalkulatorsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KALKULATORSDIALOG_H
#define UI_KALKULATORSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_KalkulatorsDialog
{
public:
    QPushButton *button7;
    QPushButton *button8;
    QPushButton *button9;
    QPushButton *button4;
    QPushButton *button1;
    QPushButton *button6;
    QPushButton *button5;
    QPushButton *button2;
    QPushButton *buttonNevienadiba;
    QPushButton *button3;
    QPushButton *button0;
    QPushButton *buttonDot;
    QPushButton *buttonTg;
    QPushButton *buttonCos;
    QPushButton *buttonSin;
    QPushButton *buttonRoot;
    QPushButton *buttonDala;
    QPushButton *buttonKapinasana;
    QPushButton *buttonProcents;
    QPushButton *buttonMinus;
    QPushButton *buttonPlus;
    QPushButton *buttonReiz;
    QPushButton *buttonDivide;
    QPushButton *buttonPi;
    QPushButton *buttonClear;
    QLabel *output;
    QPushButton *buttonEqual;

    void setupUi(QDialog *KalkulatorsDialog)
    {
        if (KalkulatorsDialog->objectName().isEmpty())
            KalkulatorsDialog->setObjectName(QString::fromUtf8("KalkulatorsDialog"));
        KalkulatorsDialog->resize(422, 431);
        button7 = new QPushButton(KalkulatorsDialog);
        button7->setObjectName(QString::fromUtf8("button7"));
        button7->setGeometry(QRect(70, 150, 71, 71));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font.setPointSize(18);
        button7->setFont(font);
        button7->setAutoFillBackground(false);
        button7->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button8 = new QPushButton(KalkulatorsDialog);
        button8->setObjectName(QString::fromUtf8("button8"));
        button8->setGeometry(QRect(140, 150, 71, 71));
        button8->setFont(font);
        button8->setAutoFillBackground(false);
        button8->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button9 = new QPushButton(KalkulatorsDialog);
        button9->setObjectName(QString::fromUtf8("button9"));
        button9->setGeometry(QRect(210, 150, 71, 71));
        button9->setFont(font);
        button9->setAutoFillBackground(false);
        button9->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);;"));
        button4 = new QPushButton(KalkulatorsDialog);
        button4->setObjectName(QString::fromUtf8("button4"));
        button4->setGeometry(QRect(70, 220, 71, 71));
        button4->setFont(font);
        button4->setAutoFillBackground(false);
        button4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button1 = new QPushButton(KalkulatorsDialog);
        button1->setObjectName(QString::fromUtf8("button1"));
        button1->setGeometry(QRect(70, 290, 71, 71));
        button1->setFont(font);
        button1->setAutoFillBackground(false);
        button1->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button6 = new QPushButton(KalkulatorsDialog);
        button6->setObjectName(QString::fromUtf8("button6"));
        button6->setGeometry(QRect(210, 220, 71, 71));
        button6->setFont(font);
        button6->setAutoFillBackground(false);
        button6->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button5 = new QPushButton(KalkulatorsDialog);
        button5->setObjectName(QString::fromUtf8("button5"));
        button5->setGeometry(QRect(140, 220, 71, 71));
        button5->setFont(font);
        button5->setAutoFillBackground(false);
        button5->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button2 = new QPushButton(KalkulatorsDialog);
        button2->setObjectName(QString::fromUtf8("button2"));
        button2->setGeometry(QRect(140, 290, 71, 71));
        button2->setFont(font);
        button2->setAutoFillBackground(false);
        button2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        buttonNevienadiba = new QPushButton(KalkulatorsDialog);
        buttonNevienadiba->setObjectName(QString::fromUtf8("buttonNevienadiba"));
        buttonNevienadiba->setGeometry(QRect(70, 360, 71, 71));
        buttonNevienadiba->setFont(font);
        buttonNevienadiba->setAutoFillBackground(false);
        buttonNevienadiba->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 127);"));
        button3 = new QPushButton(KalkulatorsDialog);
        button3->setObjectName(QString::fromUtf8("button3"));
        button3->setGeometry(QRect(210, 290, 71, 71));
        button3->setFont(font);
        button3->setAutoFillBackground(false);
        button3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        button0 = new QPushButton(KalkulatorsDialog);
        button0->setObjectName(QString::fromUtf8("button0"));
        button0->setGeometry(QRect(140, 360, 71, 71));
        button0->setFont(font);
        button0->setAutoFillBackground(false);
        button0->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        buttonDot = new QPushButton(KalkulatorsDialog);
        buttonDot->setObjectName(QString::fromUtf8("buttonDot"));
        buttonDot->setGeometry(QRect(210, 360, 71, 71));
        buttonDot->setFont(font);
        buttonDot->setAutoFillBackground(false);
        buttonDot->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 127);"));
        buttonTg = new QPushButton(KalkulatorsDialog);
        buttonTg->setObjectName(QString::fromUtf8("buttonTg"));
        buttonTg->setGeometry(QRect(210, 80, 71, 71));
        buttonTg->setFont(font);
        buttonTg->setAutoFillBackground(false);
        buttonTg->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 85, 255);"));
        buttonCos = new QPushButton(KalkulatorsDialog);
        buttonCos->setObjectName(QString::fromUtf8("buttonCos"));
        buttonCos->setGeometry(QRect(140, 80, 71, 71));
        buttonCos->setFont(font);
        buttonCos->setAutoFillBackground(false);
        buttonCos->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 85, 255);"));
        buttonSin = new QPushButton(KalkulatorsDialog);
        buttonSin->setObjectName(QString::fromUtf8("buttonSin"));
        buttonSin->setGeometry(QRect(70, 80, 71, 71));
        buttonSin->setFont(font);
        buttonSin->setAutoFillBackground(false);
        buttonSin->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 85, 255);"));
        buttonRoot = new QPushButton(KalkulatorsDialog);
        buttonRoot->setObjectName(QString::fromUtf8("buttonRoot"));
        buttonRoot->setGeometry(QRect(0, 360, 71, 71));
        buttonRoot->setFont(font);
        buttonRoot->setAutoFillBackground(false);
        buttonRoot->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 255, 255);"));
        buttonDala = new QPushButton(KalkulatorsDialog);
        buttonDala->setObjectName(QString::fromUtf8("buttonDala"));
        buttonDala->setGeometry(QRect(0, 290, 71, 71));
        buttonDala->setFont(font);
        buttonDala->setAutoFillBackground(false);
        buttonDala->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 255, 255);"));
        buttonKapinasana = new QPushButton(KalkulatorsDialog);
        buttonKapinasana->setObjectName(QString::fromUtf8("buttonKapinasana"));
        buttonKapinasana->setGeometry(QRect(0, 220, 71, 71));
        buttonKapinasana->setFont(font);
        buttonKapinasana->setAutoFillBackground(false);
        buttonKapinasana->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 255, 255);"));
        buttonProcents = new QPushButton(KalkulatorsDialog);
        buttonProcents->setObjectName(QString::fromUtf8("buttonProcents"));
        buttonProcents->setGeometry(QRect(0, 150, 71, 71));
        buttonProcents->setFont(font);
        buttonProcents->setAutoFillBackground(false);
        buttonProcents->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 255, 255);"));
        buttonMinus = new QPushButton(KalkulatorsDialog);
        buttonMinus->setObjectName(QString::fromUtf8("buttonMinus"));
        buttonMinus->setGeometry(QRect(350, 220, 71, 71));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font1.setPointSize(18);
        font1.setBold(false);
        font1.setWeight(50);
        buttonMinus->setFont(font1);
        buttonMinus->setAutoFillBackground(false);
        buttonMinus->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 127);"));
        buttonPlus = new QPushButton(KalkulatorsDialog);
        buttonPlus->setObjectName(QString::fromUtf8("buttonPlus"));
        buttonPlus->setGeometry(QRect(280, 220, 71, 71));
        buttonPlus->setFont(font);
        buttonPlus->setAutoFillBackground(false);
        buttonPlus->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 127);"));
        buttonReiz = new QPushButton(KalkulatorsDialog);
        buttonReiz->setObjectName(QString::fromUtf8("buttonReiz"));
        buttonReiz->setGeometry(QRect(280, 150, 71, 71));
        buttonReiz->setFont(font);
        buttonReiz->setAutoFillBackground(false);
        buttonReiz->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 127);"));
        buttonDivide = new QPushButton(KalkulatorsDialog);
        buttonDivide->setObjectName(QString::fromUtf8("buttonDivide"));
        buttonDivide->setGeometry(QRect(350, 150, 71, 71));
        buttonDivide->setFont(font);
        buttonDivide->setAutoFillBackground(false);
        buttonDivide->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 127);"));
        buttonPi = new QPushButton(KalkulatorsDialog);
        buttonPi->setObjectName(QString::fromUtf8("buttonPi"));
        buttonPi->setGeometry(QRect(0, 80, 71, 71));
        buttonPi->setFont(font);
        buttonPi->setAutoFillBackground(false);
        buttonPi->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 85, 255);"));
        buttonClear = new QPushButton(KalkulatorsDialog);
        buttonClear->setObjectName(QString::fromUtf8("buttonClear"));
        buttonClear->setGeometry(QRect(280, 80, 141, 71));
        buttonClear->setFont(font);
        buttonClear->setAutoFillBackground(false);
        buttonClear->setStyleSheet(QString::fromUtf8("background-color:rgb(255, 60, 60);"));
        output = new QLabel(KalkulatorsDialog);
        output->setObjectName(QString::fromUtf8("output"));
        output->setGeometry(QRect(0, 0, 421, 81));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font2.setPointSize(26);
        font2.setBold(false);
        font2.setWeight(50);
        output->setFont(font2);
        output->setStyleSheet(QString::fromUtf8(" qproperty-alignment: 'AlignVCenter | AlignRight';\n"
"  border: 1px solid gray;"));
        buttonEqual = new QPushButton(KalkulatorsDialog);
        buttonEqual->setObjectName(QString::fromUtf8("buttonEqual"));
        buttonEqual->setGeometry(QRect(280, 290, 141, 141));
        buttonEqual->setFont(font);
        buttonEqual->setAutoFillBackground(false);
        buttonEqual->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 127);"));

        retranslateUi(KalkulatorsDialog);

        QMetaObject::connectSlotsByName(KalkulatorsDialog);
    } // setupUi

    void retranslateUi(QDialog *KalkulatorsDialog)
    {
        KalkulatorsDialog->setWindowTitle(QApplication::translate("KalkulatorsDialog", "Dialog", nullptr));
        button7->setText(QApplication::translate("KalkulatorsDialog", "7", nullptr));
        button8->setText(QApplication::translate("KalkulatorsDialog", "8", nullptr));
        button9->setText(QApplication::translate("KalkulatorsDialog", "9", nullptr));
        button4->setText(QApplication::translate("KalkulatorsDialog", "4", nullptr));
        button1->setText(QApplication::translate("KalkulatorsDialog", "1", nullptr));
        button6->setText(QApplication::translate("KalkulatorsDialog", "6", nullptr));
        button5->setText(QApplication::translate("KalkulatorsDialog", "5", nullptr));
        button2->setText(QApplication::translate("KalkulatorsDialog", "2", nullptr));
        buttonNevienadiba->setText(QApplication::translate("KalkulatorsDialog", "+/-", nullptr));
        button3->setText(QApplication::translate("KalkulatorsDialog", "3", nullptr));
        button0->setText(QApplication::translate("KalkulatorsDialog", "0", nullptr));
        buttonDot->setText(QApplication::translate("KalkulatorsDialog", ".", nullptr));
        buttonTg->setText(QApplication::translate("KalkulatorsDialog", "tg", nullptr));
        buttonCos->setText(QApplication::translate("KalkulatorsDialog", "cos", nullptr));
        buttonSin->setText(QApplication::translate("KalkulatorsDialog", "sin", nullptr));
        buttonRoot->setText(QApplication::translate("KalkulatorsDialog", "\342\210\232x", nullptr));
        buttonDala->setText(QApplication::translate("KalkulatorsDialog", "1/X", nullptr));
        buttonKapinasana->setText(QApplication::translate("KalkulatorsDialog", "X^y", nullptr));
        buttonProcents->setText(QApplication::translate("KalkulatorsDialog", "%", nullptr));
        buttonMinus->setText(QApplication::translate("KalkulatorsDialog", "-", nullptr));
        buttonPlus->setText(QApplication::translate("KalkulatorsDialog", "+", nullptr));
        buttonReiz->setText(QApplication::translate("KalkulatorsDialog", "*", nullptr));
        buttonDivide->setText(QApplication::translate("KalkulatorsDialog", "/", nullptr));
        buttonPi->setText(QApplication::translate("KalkulatorsDialog", "\317\200", nullptr));
        buttonClear->setText(QApplication::translate("KalkulatorsDialog", "C", nullptr));
        output->setText(QApplication::translate("KalkulatorsDialog", "0", nullptr));
        buttonEqual->setText(QApplication::translate("KalkulatorsDialog", "=", nullptr));
    } // retranslateUi

};

namespace Ui {
    class KalkulatorsDialog: public Ui_KalkulatorsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KALKULATORSDIALOG_H

#ifndef KALKULATORSDIALOG_H
#define KALKULATORSDIALOG_H

#include <QDialog>

namespace Ui {
class KalkulatorsDialog;
}

class KalkulatorsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KalkulatorsDialog(QWidget *parent = nullptr);
    ~KalkulatorsDialog();

private slots:
   void digitPressed();

   void on_buttonDot_released();

   void on_buttonNevienadiba_released();

   void on_buttonClear_released();

   void on_buttonProcents_released();

   void on_buttonDala_released();

   void on_buttonPi_released();

   void vienkVienadojumi();


   void on_buttonEqual_released();

   void on_buttonRoot_released();

   void on_buttonSin_released();

   void on_buttonCos_released();

   void on_buttonTg_released();

private:
    Ui::KalkulatorsDialog *ui;
};

#endif // KALKULATORSDIALOG_H

#include "datuparvdialog.h"
#include "ui_datuparvdialog.h"
#include <QMessageBox>
#include <iostream>
#include <string>
DatuParvDialog::DatuParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatuParvDialog)
{
    ui->setupUi(this);

    QStringList Names= {"B","KB","MB","GB","TB"};
    foreach (QString item, Names)
    {
        ui->comboBoxDati1->addItem(item);
        ui->comboBoxDati2->addItem(item);
    }
    //pievieno comboBox šos nosaukumus
}

DatuParvDialog::~DatuParvDialog()
{
    delete ui;
}

void DatuParvDialog::on_DatuParvPushButton_clicked()
{
    const double b =1099511627776;
    const double kB = 1073741824;
    const double mB =1048576;
    const double gB =1024;
    const double tB =1;

    double value;
    QString temp1,temp2;

    temp1 =ui->comboBoxDati1->currentText();
    temp2 =ui->comboBoxDati2->currentText();
    value = ui->DatuInput->value();
    if (temp1 == "B")
        value = value /b;

    else if (temp1 == "KB")
    {
        value = value/kB;
    }

    else if (temp1 == "MB")
    {
        value = value/mB;
    }

    else if (temp1 == "GB")
    {
         value = value/gB;
    }

    else if (temp1 == "TB")
    {
        value = value/tB;
    }



    if (temp2 == "B")
        value = value*b;

    else if (temp2 == "KB")
    {
        value = value*kB;
    }

    else if (temp2 == "MB")
    {
        value = value*mB;
    }

    else if (temp2 == "GB")
    {
         value = value*gB;
    }

    else if (temp2 == "TB")
    {
        value = value*tB;
    }



   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->DatuOutput->setText(RealOutPut);
}

void DatuParvDialog::on_DatuInput_valueChanged(double arg1)
{

}

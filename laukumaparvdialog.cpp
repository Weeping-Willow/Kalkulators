#include "laukumaparvdialog.h"
#include "ui_laukumaparvdialog.h"

LaukumaParvDialog::LaukumaParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LaukumaParvDialog)
{
    ui->setupUi(this);
    QStringList Names= {"Kvadrātmetri","Kvadrātpēdas","Kvadrātjūdzas","Akri","Hektāri","Kvadrātkilometri"};
    foreach (QString item, Names) {
        ui->comboBoxLaukuma1->addItem(item);
        ui->comboBoxLaukuma2->addItem(item);
    }
}

LaukumaParvDialog::~LaukumaParvDialog()
{
    delete ui;
}


void LaukumaParvDialog::on_LaukumaParvPushButton_clicked()
{
    const double kvadratmetri =2589988.11034;
    const double kvadratpedas = 27878400;
    const double kvadratjudzas =1;
    const double akri = 640;
    const double hektari =258.99881;
    const double kvadratkilometri =2.58999;


    double value;
    QString temp1,temp2;

    temp1 =ui->comboBoxLaukuma1->currentText();
    temp2 =ui->comboBoxLaukuma2->currentText();
    value = ui->LaukumaInput->value();

    if (temp1 == "Kvadrātmetri")
        value = value /kvadratmetri;

    else if (temp1 == "Kvadrātpēdas")
    {
        value = value/kvadratpedas;
    }

    else if (temp1 == "Kvadrātjūdzas")
    {
        value = value/kvadratjudzas;
    }

    else if (temp1 == "Akri")
    {
         value = value/akri;
    }

    else if (temp1 == "Hektāri")
    {
        value = value/hektari;
    }
    else if (temp1 == "Kvadrātkilometri")
    {
        value = value/kvadratkilometri;
    }



    if (temp2 == "Kvadrātmetri")
        value = value *kvadratmetri;

    else if (temp2 == "Kvadrātpēdas")
    {
        value = value*kvadratpedas;
    }

    else if (temp2 == "Kvadrātjūdzas")
    {
        value = value*kvadratjudzas;
    }

    else if (temp2 == "Akri")
    {
         value = value*akri;
    }

    else if (temp2 == "Hektāri")
    {
        value = value*hektari;
    }

    else if (temp2 == "Kvadrātkilometri")
    {
        value = value*kvadratkilometri;
    }




   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->LaukumaOutput->setText(RealOutPut);
}

/********************************************************************************
** Form generated from reading UI file 'kalkulatoraspeledialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KALKULATORASPELEDIALOG_H
#define UI_KALKULATORASPELEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_KalkulatoraSpeleDialog
{
public:
    QPushButton *buttonSpelet;
    QPushButton *buttonEnd;
    QLabel *label_3;
    QLCDNumber *time;
    QLabel *label_2;
    QLabel *Output;
    QPushButton *checkIfCorrect;
    QLCDNumber *score;
    QComboBox *comboBoxType;
    QComboBox *comboBoxGrutiba;
    QComboBox *comboBoxTime;
    QLineEdit *Input;
    QPushButton *pushButton;

    void setupUi(QDialog *KalkulatoraSpeleDialog)
    {
        if (KalkulatoraSpeleDialog->objectName().isEmpty())
            KalkulatoraSpeleDialog->setObjectName(QString::fromUtf8("KalkulatoraSpeleDialog"));
        KalkulatoraSpeleDialog->resize(408, 383);
        buttonSpelet = new QPushButton(KalkulatoraSpeleDialog);
        buttonSpelet->setObjectName(QString::fromUtf8("buttonSpelet"));
        buttonSpelet->setGeometry(QRect(70, 133, 281, 71));
        buttonEnd = new QPushButton(KalkulatoraSpeleDialog);
        buttonEnd->setObjectName(QString::fromUtf8("buttonEnd"));
        buttonEnd->setGeometry(QRect(340, 0, 61, 31));
        label_3 = new QLabel(KalkulatoraSpeleDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(230, 20, 111, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font.setPointSize(18);
        label_3->setFont(font);
        time = new QLCDNumber(KalkulatoraSpeleDialog);
        time->setObjectName(QString::fromUtf8("time"));
        time->setGeometry(QRect(10, 40, 171, 91));
        time->setStyleSheet(QString::fromUtf8(""));
        time->setFrameShadow(QFrame::Plain);
        time->setLineWidth(2);
        label_2 = new QLabel(KalkulatoraSpeleDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 20, 171, 20));
        label_2->setFont(font);
        Output = new QLabel(KalkulatoraSpeleDialog);
        Output->setObjectName(QString::fromUtf8("Output"));
        Output->setGeometry(QRect(10, 130, 391, 71));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font1.setPointSize(20);
        Output->setFont(font1);
        Output->setStyleSheet(QString::fromUtf8(" qproperty-alignment: 'AlignVCenter | AlignRight';\n"
"  border: 1px solid gray;"));
        checkIfCorrect = new QPushButton(KalkulatoraSpeleDialog);
        checkIfCorrect->setObjectName(QString::fromUtf8("checkIfCorrect"));
        checkIfCorrect->setGeometry(QRect(140, 300, 131, 51));
        score = new QLCDNumber(KalkulatoraSpeleDialog);
        score->setObjectName(QString::fromUtf8("score"));
        score->setGeometry(QRect(230, 40, 171, 91));
        score->setLayoutDirection(Qt::LeftToRight);
        score->setFrameShadow(QFrame::Plain);
        score->setLineWidth(2);
        score->setMidLineWidth(-2);
        comboBoxType = new QComboBox(KalkulatoraSpeleDialog);
        comboBoxType->setObjectName(QString::fromUtf8("comboBoxType"));
        comboBoxType->setGeometry(QRect(1, 92, 131, 32));
        comboBoxGrutiba = new QComboBox(KalkulatoraSpeleDialog);
        comboBoxGrutiba->setObjectName(QString::fromUtf8("comboBoxGrutiba"));
        comboBoxGrutiba->setGeometry(QRect(136, 92, 131, 32));
        comboBoxTime = new QComboBox(KalkulatoraSpeleDialog);
        comboBoxTime->setObjectName(QString::fromUtf8("comboBoxTime"));
        comboBoxTime->setGeometry(QRect(271, 92, 131, 32));
        Input = new QLineEdit(KalkulatoraSpeleDialog);
        Input->setObjectName(QString::fromUtf8("Input"));
        Input->setGeometry(QRect(10, 200, 391, 81));
        pushButton = new QPushButton(KalkulatoraSpeleDialog);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(320, 350, 91, 31));

        retranslateUi(KalkulatoraSpeleDialog);

        QMetaObject::connectSlotsByName(KalkulatoraSpeleDialog);
    } // setupUi

    void retranslateUi(QDialog *KalkulatoraSpeleDialog)
    {
        KalkulatoraSpeleDialog->setWindowTitle(QApplication::translate("KalkulatoraSpeleDialog", "Dialog", nullptr));
        buttonSpelet->setText(QApplication::translate("KalkulatoraSpeleDialog", "Spelet", nullptr));
        buttonEnd->setText(QApplication::translate("KalkulatoraSpeleDialog", "Beigt", nullptr));
        label_3->setText(QApplication::translate("KalkulatoraSpeleDialog", "Rezultats", nullptr));
        label_2->setText(QApplication::translate("KalkulatoraSpeleDialog", "Laiks", nullptr));
        Output->setText(QString());
        checkIfCorrect->setText(QApplication::translate("KalkulatoraSpeleDialog", "Parbaudit", nullptr));
        pushButton->setText(QApplication::translate("KalkulatoraSpeleDialog", "Instrukcijas", nullptr));
    } // retranslateUi

};

namespace Ui {
    class KalkulatoraSpeleDialog: public Ui_KalkulatoraSpeleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KALKULATORASPELEDIALOG_H

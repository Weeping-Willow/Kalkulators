/********************************************************************************
** Form generated from reading UI file 'tilpumaparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TILPUMAPARVDIALOG_H
#define UI_TILPUMAPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TilpumaParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *TilpumaInput;
    QComboBox *comboBoxTilpuma1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *TilpumaOutput;
    QComboBox *comboBoxTilpuma2;
    QPushButton *TilpumaParvPushButton;

    void setupUi(QDialog *TilpumaParvDialog)
    {
        if (TilpumaParvDialog->objectName().isEmpty())
            TilpumaParvDialog->setObjectName(QString::fromUtf8("TilpumaParvDialog"));
        TilpumaParvDialog->resize(387, 292);
        layoutWidget = new QWidget(TilpumaParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(40, 40, 331, 211));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        TilpumaInput = new QDoubleSpinBox(layoutWidget);
        TilpumaInput->setObjectName(QString::fromUtf8("TilpumaInput"));
        TilpumaInput->setDecimals(6);
        TilpumaInput->setMaximum(9999999999.999998092651367);

        horizontalLayout_3->addWidget(TilpumaInput);

        comboBoxTilpuma1 = new QComboBox(layoutWidget);
        comboBoxTilpuma1->setObjectName(QString::fromUtf8("comboBoxTilpuma1"));

        horizontalLayout_3->addWidget(comboBoxTilpuma1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        TilpumaOutput = new QLineEdit(layoutWidget);
        TilpumaOutput->setObjectName(QString::fromUtf8("TilpumaOutput"));

        horizontalLayout_4->addWidget(TilpumaOutput);

        comboBoxTilpuma2 = new QComboBox(layoutWidget);
        comboBoxTilpuma2->setObjectName(QString::fromUtf8("comboBoxTilpuma2"));

        horizontalLayout_4->addWidget(comboBoxTilpuma2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        TilpumaParvPushButton = new QPushButton(layoutWidget);
        TilpumaParvPushButton->setObjectName(QString::fromUtf8("TilpumaParvPushButton"));

        verticalLayout->addWidget(TilpumaParvPushButton);


        retranslateUi(TilpumaParvDialog);

        QMetaObject::connectSlotsByName(TilpumaParvDialog);
    } // setupUi

    void retranslateUi(QDialog *TilpumaParvDialog)
    {
        TilpumaParvDialog->setWindowTitle(QApplication::translate("TilpumaParvDialog", "Dialog", nullptr));
        TilpumaParvPushButton->setText(QApplication::translate("TilpumaParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TilpumaParvDialog: public Ui_TilpumaParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TILPUMAPARVDIALOG_H

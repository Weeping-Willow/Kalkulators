#ifndef VALUTASPARVDIALOG_H
#define VALUTASPARVDIALOG_H

#include <QDialog>
#include <QNetworkAccessManager>

namespace Ui {
class ValutasParvDialog;
}

class ValutasParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ValutasParvDialog(QWidget *parent = nullptr);
    ~ValutasParvDialog();

private slots:


    void fileIsReady( QNetworkReply * reply);
    void on_TilpumaParvPushButton_clicked();

private:
    Ui::ValutasParvDialog *ui;

    QNetworkAccessManager * netManager;
    QNetworkReply * netReply;
};

#endif // VALUTASPARVDIALOG_H

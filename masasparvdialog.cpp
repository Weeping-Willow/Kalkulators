#include "masasparvdialog.h"
#include "ui_masasparvdialog.h"

MasasParvDialog::MasasParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MasasParvDialog)
{
    ui->setupUi(this);
    QStringList Names= {"Grami","Kilogrami","Tonnas","Karāti","Miligrami","Mārciņas"};
    foreach (QString item, Names) {
        ui->comboBoxMasas1->addItem(item);
        ui->comboBoxMasas2->addItem(item);
    }
}

MasasParvDialog::~MasasParvDialog()
{
    delete ui;
}

void MasasParvDialog::on_MasasParvPushButton_clicked()
{
    const double grami =1000000;
    const double kilogrami = 1000;
    const double tonnas =1;
    const double karati = 5000000;
    const double miligrami =1000000000;
    const double marcinas =2204.62262;


    double value;
    QString temp1,temp2;

    temp1 =ui->comboBoxMasas1->currentText();
    temp2 =ui->comboBoxMasas2->currentText();
    value = ui->MasasInput->value();

    if (temp1 == "Grami")
        value = value /grami;

    else if (temp1 == "Kilogrami")
    {
        value = value/kilogrami;
    }

    else if (temp1 == "Tonnas")
    {
        value = value/tonnas;
    }

    else if (temp1 == "Karāti")
    {
         value = value/karati;
    }

    else if (temp1 == "Miligrami")
    {
        value = value/miligrami;
    }
    else if (temp1 == "Mārciņas")
    {
        value = value/marcinas;
    }



    if (temp2 == "Grami")
        value = value *grami;

    else if (temp2 == "Kilogrami")
    {
        value = value*kilogrami;
    }

    else if (temp2 == "Tonnas")
    {
        value = value*tonnas;
    }

    else if (temp2 == "Karāti")
    {
         value = value*karati;
    }

    else if (temp2 == "Miligrami")
    {
        value = value*miligrami;
    }

    else if (temp2 == "Mārciņas")
    {
        value = value*marcinas;
    }




   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->MasasOutput->setText(RealOutPut);
}

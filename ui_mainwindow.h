/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *FirstRow;
    QPushButton *StandartCalculatorButton;
    QPushButton *SpelesButton;
    QHBoxLayout *SecondRow;
    QPushButton *AtrumaParvButton;
    QPushButton *DatumaParvButton;
    QPushButton *DatuParvButton;
    QHBoxLayout *ThridRow;
    QPushButton *GarumaParvButton;
    QPushButton *TempParvButton;
    QPushButton *LaukumaParvButton;
    QHBoxLayout *FourthRow;
    QPushButton *MasasParvButton;
    QPushButton *TilpumaParvButton;
    QPushButton *ValutasParvButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(407, 312);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(30, 0, 341, 241));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        FirstRow = new QHBoxLayout();
        FirstRow->setSpacing(6);
        FirstRow->setObjectName(QString::fromUtf8("FirstRow"));
        StandartCalculatorButton = new QPushButton(widget);
        StandartCalculatorButton->setObjectName(QString::fromUtf8("StandartCalculatorButton"));

        FirstRow->addWidget(StandartCalculatorButton);

        SpelesButton = new QPushButton(widget);
        SpelesButton->setObjectName(QString::fromUtf8("SpelesButton"));

        FirstRow->addWidget(SpelesButton);


        verticalLayout->addLayout(FirstRow);

        SecondRow = new QHBoxLayout();
        SecondRow->setSpacing(6);
        SecondRow->setObjectName(QString::fromUtf8("SecondRow"));
        AtrumaParvButton = new QPushButton(widget);
        AtrumaParvButton->setObjectName(QString::fromUtf8("AtrumaParvButton"));

        SecondRow->addWidget(AtrumaParvButton);

        DatumaParvButton = new QPushButton(widget);
        DatumaParvButton->setObjectName(QString::fromUtf8("DatumaParvButton"));

        SecondRow->addWidget(DatumaParvButton);

        DatuParvButton = new QPushButton(widget);
        DatuParvButton->setObjectName(QString::fromUtf8("DatuParvButton"));

        SecondRow->addWidget(DatuParvButton);


        verticalLayout->addLayout(SecondRow);

        ThridRow = new QHBoxLayout();
        ThridRow->setSpacing(6);
        ThridRow->setObjectName(QString::fromUtf8("ThridRow"));
        GarumaParvButton = new QPushButton(widget);
        GarumaParvButton->setObjectName(QString::fromUtf8("GarumaParvButton"));

        ThridRow->addWidget(GarumaParvButton);

        TempParvButton = new QPushButton(widget);
        TempParvButton->setObjectName(QString::fromUtf8("TempParvButton"));

        ThridRow->addWidget(TempParvButton);

        LaukumaParvButton = new QPushButton(widget);
        LaukumaParvButton->setObjectName(QString::fromUtf8("LaukumaParvButton"));

        ThridRow->addWidget(LaukumaParvButton);


        verticalLayout->addLayout(ThridRow);

        FourthRow = new QHBoxLayout();
        FourthRow->setSpacing(6);
        FourthRow->setObjectName(QString::fromUtf8("FourthRow"));
        MasasParvButton = new QPushButton(widget);
        MasasParvButton->setObjectName(QString::fromUtf8("MasasParvButton"));

        FourthRow->addWidget(MasasParvButton);

        TilpumaParvButton = new QPushButton(widget);
        TilpumaParvButton->setObjectName(QString::fromUtf8("TilpumaParvButton"));

        FourthRow->addWidget(TilpumaParvButton);

        ValutasParvButton = new QPushButton(widget);
        ValutasParvButton->setObjectName(QString::fromUtf8("ValutasParvButton"));

        FourthRow->addWidget(ValutasParvButton);


        verticalLayout->addLayout(FourthRow);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 407, 30));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        StandartCalculatorButton->setText(QApplication::translate("MainWindow", "Standarts", nullptr));
        SpelesButton->setText(QApplication::translate("MainWindow", "Spele", nullptr));
        AtrumaParvButton->setText(QApplication::translate("MainWindow", "Atruma parv.", nullptr));
        DatumaParvButton->setText(QApplication::translate("MainWindow", "Datuma parv.", nullptr));
        DatuParvButton->setText(QApplication::translate("MainWindow", "Datu parv.", nullptr));
        GarumaParvButton->setText(QApplication::translate("MainWindow", "Garuma parv.", nullptr));
        TempParvButton->setText(QApplication::translate("MainWindow", "Temp parv.", nullptr));
        LaukumaParvButton->setText(QApplication::translate("MainWindow", "Laukuma parv.", nullptr));
        MasasParvButton->setText(QApplication::translate("MainWindow", "Masas parv.", nullptr));
        TilpumaParvButton->setText(QApplication::translate("MainWindow", "Tilpuma parv.", nullptr));
        ValutasParvButton->setText(QApplication::translate("MainWindow", "Valutas parv.", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

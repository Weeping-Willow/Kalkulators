/********************************************************************************
** Form generated from reading UI file 'datumaparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATUMAPARVDIALOG_H
#define UI_DATUMAPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DatumaParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *Izvade1;
    QLabel *Izvade2;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QLabel *Datums1;
    QLabel *Datums2;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QCalendarWidget *calendarWidget;
    QPushButton *ievadeDatums1Poga;
    QPushButton *izvadeDatums2Poga;
    QPushButton *ApreikinasanasPoga;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout;
    QLineEdit *StarpibaApreikinasanasOutput;
    QComboBox *FormataIzveleComboBox;
    QCheckBox *checkBox;

    void setupUi(QDialog *DatumaParvDialog)
    {
        if (DatumaParvDialog->objectName().isEmpty())
            DatumaParvDialog->setObjectName(QString::fromUtf8("DatumaParvDialog"));
        DatumaParvDialog->resize(741, 391);
        layoutWidget = new QWidget(DatumaParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(450, 10, 111, 141));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        Izvade1 = new QLabel(layoutWidget);
        Izvade1->setObjectName(QString::fromUtf8("Izvade1"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        Izvade1->setFont(font);

        verticalLayout_2->addWidget(Izvade1);

        Izvade2 = new QLabel(layoutWidget);
        Izvade2->setObjectName(QString::fromUtf8("Izvade2"));
        Izvade2->setFont(font);

        verticalLayout_2->addWidget(Izvade2);

        layoutWidget1 = new QWidget(DatumaParvDialog);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(570, 10, 161, 141));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        Datums1 = new QLabel(layoutWidget1);
        Datums1->setObjectName(QString::fromUtf8("Datums1"));
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        Datums1->setFont(font1);

        verticalLayout->addWidget(Datums1);

        Datums2 = new QLabel(layoutWidget1);
        Datums2->setObjectName(QString::fromUtf8("Datums2"));
        Datums2->setFont(font1);

        verticalLayout->addWidget(Datums2);

        layoutWidget2 = new QWidget(DatumaParvDialog);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(0, 10, 444, 364));
        verticalLayout_4 = new QVBoxLayout(layoutWidget2);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        calendarWidget = new QCalendarWidget(layoutWidget2);
        calendarWidget->setObjectName(QString::fromUtf8("calendarWidget"));

        verticalLayout_3->addWidget(calendarWidget);

        ievadeDatums1Poga = new QPushButton(layoutWidget2);
        ievadeDatums1Poga->setObjectName(QString::fromUtf8("ievadeDatums1Poga"));

        verticalLayout_3->addWidget(ievadeDatums1Poga);

        izvadeDatums2Poga = new QPushButton(layoutWidget2);
        izvadeDatums2Poga->setObjectName(QString::fromUtf8("izvadeDatums2Poga"));

        verticalLayout_3->addWidget(izvadeDatums2Poga);


        verticalLayout_4->addLayout(verticalLayout_3);

        ApreikinasanasPoga = new QPushButton(layoutWidget2);
        ApreikinasanasPoga->setObjectName(QString::fromUtf8("ApreikinasanasPoga"));

        verticalLayout_4->addWidget(ApreikinasanasPoga);

        layoutWidget3 = new QWidget(DatumaParvDialog);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(450, 170, 271, 34));
        horizontalLayout = new QHBoxLayout(layoutWidget3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        StarpibaApreikinasanasOutput = new QLineEdit(layoutWidget3);
        StarpibaApreikinasanasOutput->setObjectName(QString::fromUtf8("StarpibaApreikinasanasOutput"));

        horizontalLayout->addWidget(StarpibaApreikinasanasOutput);

        FormataIzveleComboBox = new QComboBox(layoutWidget3);
        FormataIzveleComboBox->setObjectName(QString::fromUtf8("FormataIzveleComboBox"));

        horizontalLayout->addWidget(FormataIzveleComboBox);

        checkBox = new QCheckBox(DatumaParvDialog);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(450, 220, 281, 22));

        retranslateUi(DatumaParvDialog);

        QMetaObject::connectSlotsByName(DatumaParvDialog);
    } // setupUi

    void retranslateUi(QDialog *DatumaParvDialog)
    {
        DatumaParvDialog->setWindowTitle(QApplication::translate("DatumaParvDialog", "Dialog", nullptr));
        Izvade1->setText(QApplication::translate("DatumaParvDialog", "1.Datums", nullptr));
        Izvade2->setText(QApplication::translate("DatumaParvDialog", "2.Datums", nullptr));
        Datums1->setText(QString());
        Datums2->setText(QString());
        ievadeDatums1Poga->setText(QApplication::translate("DatumaParvDialog", "1.Datums", nullptr));
        izvadeDatums2Poga->setText(QApplication::translate("DatumaParvDialog", "2.Datums", nullptr));
        ApreikinasanasPoga->setText(QApplication::translate("DatumaParvDialog", "Aprei\304\267inat", nullptr));
        checkBox->setText(QApplication::translate("DatumaParvDialog", "Ieskait\304\253t p\304\223d\304\223jo dienu", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DatumaParvDialog: public Ui_DatumaParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATUMAPARVDIALOG_H

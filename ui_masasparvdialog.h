/********************************************************************************
** Form generated from reading UI file 'masasparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MASASPARVDIALOG_H
#define UI_MASASPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MasasParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *MasasInput;
    QComboBox *comboBoxMasas1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *MasasOutput;
    QComboBox *comboBoxMasas2;
    QPushButton *MasasParvPushButton;

    void setupUi(QDialog *MasasParvDialog)
    {
        if (MasasParvDialog->objectName().isEmpty())
            MasasParvDialog->setObjectName(QString::fromUtf8("MasasParvDialog"));
        MasasParvDialog->resize(400, 300);
        layoutWidget = new QWidget(MasasParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 40, 331, 211));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        MasasInput = new QDoubleSpinBox(layoutWidget);
        MasasInput->setObjectName(QString::fromUtf8("MasasInput"));
        MasasInput->setDecimals(6);
        MasasInput->setMaximum(9999999999.999998092651367);

        horizontalLayout_3->addWidget(MasasInput);

        comboBoxMasas1 = new QComboBox(layoutWidget);
        comboBoxMasas1->setObjectName(QString::fromUtf8("comboBoxMasas1"));

        horizontalLayout_3->addWidget(comboBoxMasas1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        MasasOutput = new QLineEdit(layoutWidget);
        MasasOutput->setObjectName(QString::fromUtf8("MasasOutput"));

        horizontalLayout_4->addWidget(MasasOutput);

        comboBoxMasas2 = new QComboBox(layoutWidget);
        comboBoxMasas2->setObjectName(QString::fromUtf8("comboBoxMasas2"));

        horizontalLayout_4->addWidget(comboBoxMasas2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        MasasParvPushButton = new QPushButton(layoutWidget);
        MasasParvPushButton->setObjectName(QString::fromUtf8("MasasParvPushButton"));

        verticalLayout->addWidget(MasasParvPushButton);


        retranslateUi(MasasParvDialog);

        QMetaObject::connectSlotsByName(MasasParvDialog);
    } // setupUi

    void retranslateUi(QDialog *MasasParvDialog)
    {
        MasasParvDialog->setWindowTitle(QApplication::translate("MasasParvDialog", "Dialog", nullptr));
        MasasParvPushButton->setText(QApplication::translate("MasasParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MasasParvDialog: public Ui_MasasParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MASASPARVDIALOG_H

#ifndef DATUMAPARVDIALOG_H
#define DATUMAPARVDIALOG_H

#include <QDialog>

namespace Ui {
class DatumaParvDialog;
}

class DatumaParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DatumaParvDialog(QWidget *parent = nullptr);
    ~DatumaParvDialog();
    int numberOfDaysThatAreWorkOrFree(bool isWorkDay,int diffrenceBetweenDays);

private slots:


    void on_ievadeDatums1Poga_clicked();

    void on_izvadeDatums2Poga_clicked();

    void on_ApreikinasanasPoga_clicked();

private:
    Ui::DatumaParvDialog *ui;
};

#endif // DATUMAPARVDIALOG_H

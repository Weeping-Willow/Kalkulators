#ifndef GARUMAPARVDIALOG_H
#define GARUMAPARVDIALOG_H

#include <QDialog>

namespace Ui {
class GarumaParvDialog;
}

class GarumaParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GarumaParvDialog(QWidget *parent = nullptr);
    ~GarumaParvDialog();

private slots:
    void on_GarumsParvPushButton_clicked();

private:
    Ui::GarumaParvDialog *ui;
};

#endif // GARUMAPARVDIALOG_H

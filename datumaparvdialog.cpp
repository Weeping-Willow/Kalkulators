#include "datumaparvdialog.h"
#include "ui_datumaparvdialog.h"
#include <QMessageBox>
#include <QDebug>


QDate tempDatums1,tempDatums2;
DatumaParvDialog::DatumaParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatumaParvDialog)
{
    ui->setupUi(this);

    QStringList Names= {"Dienas","Nedēļas","Mēneši","Gadi","Darbadienas","Brīvdienas"};
    foreach (QString item, Names)
    {
        ui->FormataIzveleComboBox->addItem(item);
    }
    //pievieno comboBox šos nosaukumus


}

DatumaParvDialog::~DatumaParvDialog()
{
    delete ui;
}


void DatumaParvDialog::on_ievadeDatums1Poga_clicked()
{


    tempDatums1 = ui->calendarWidget->selectedDate();
    QString temp1= tempDatums1.toString("dd-MM-yyyy");
    ui->Datums1->setText(temp1);

}

void DatumaParvDialog::on_izvadeDatums2Poga_clicked()
{

    tempDatums2 = ui->calendarWidget->selectedDate();
    QString temp1= tempDatums2.toString("dd-MM-yyyy");
    ui->Datums2->setText(temp1);

}

void DatumaParvDialog::on_ApreikinasanasPoga_clicked()
{
    if (tempDatums1.isNull() || tempDatums2.isNull() )
        QMessageBox::warning(this,"Nav ievadee","Jus nesat izvēlējušies 2 datumus");

    else
    {
        int isChecked =0;
        if (ui->checkBox->checkState())
            isChecked = 1;

        QString format = ui->FormataIzveleComboBox->currentText();
        float  difference = tempDatums1.daysTo(tempDatums2)+isChecked;

        if(format == "Dienas")
            difference = difference;

        else if(format =="Nedēļas")
            difference = difference/7;

        else if(format =="Mēneši")
            difference = difference/30;

        else if(format =="Gadi")
            difference = difference/365.25;

        else if(format == "Darbadienas")
        {
            difference =numberOfDaysThatAreWorkOrFree(true,difference);

        }

        else if(format == "Brīvdienas")
        {
            difference=numberOfDaysThatAreWorkOrFree(false,difference);
        }


        QString differenceOutput = QString::number(difference);
        ui->StarpibaApreikinasanasOutput->setText(differenceOutput);
    }
}

int DatumaParvDialog::numberOfDaysThatAreWorkOrFree(bool isWorkDay,int diffrenceBetweenDays)
{
    int workOrFreeDayCount = 0;
    qDebug()<<"diffrence is "<<diffrenceBetweenDays;
    if (isWorkDay)
    {
        for (int var = 0; var < diffrenceBetweenDays; var++)
        {
            if(tempDatums1.addDays(var).dayOfWeek()== 1|| tempDatums1.addDays(var).dayOfWeek()== 2||tempDatums1.addDays(var).dayOfWeek()== 3||tempDatums1.addDays(var).dayOfWeek()== 4||tempDatums1.addDays(var).dayOfWeek()== 5)
                workOrFreeDayCount++;


        }
    }
    else
    {
        for (int var = 0; var < diffrenceBetweenDays; var++)
        {
            if(tempDatums1.addDays(var).dayOfWeek()== 6|| tempDatums1.addDays(var).dayOfWeek()== 7)
                workOrFreeDayCount++;
        }
    }
    return workOrFreeDayCount;
}

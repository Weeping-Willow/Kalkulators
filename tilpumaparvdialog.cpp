#include "tilpumaparvdialog.h"
#include "ui_tilpumaparvdialog.h"

TilpumaParvDialog::TilpumaParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TilpumaParvDialog)
{
    ui->setupUi(this);
    QStringList Names= {"Litri","Mililitri","Kubikmetrs","Galons","Pintes","NaftasBarerls"};
    foreach (QString item, Names)
    {
        ui->comboBoxTilpuma1->addItem(item);
        ui->comboBoxTilpuma2->addItem(item);
    }
    //pievieno comboBox šos nosaukumus
}

TilpumaParvDialog::~TilpumaParvDialog()
{
    delete ui;

}

void TilpumaParvDialog::on_TilpumaParvPushButton_clicked()
{
    const double litri =1000;
    const double mililitri = 1000000;
    const double kubikmetrs =1;
    const double galons = 219.96925;
    const double pintes =1759.75399;
    const double naftasBarerls =6.28981;


    double value;
    QString temp1,temp2;

    temp1 =ui->comboBoxTilpuma1->currentText();
    temp2 =ui->comboBoxTilpuma2->currentText();
    value = ui->TilpumaInput->value();

    if (temp1 == "Litri")
        value = value /litri;

    else if (temp1 == "Mililitri")
    {
        value = value/mililitri;
    }

    else if (temp1 == "Kubikmetrs")
    {
        value = value/kubikmetrs;
    }

    else if (temp1 == "Galons")
    {
         value = value/galons;
    }

    else if (temp1 == "Pintes")
    {
        value = value/pintes;
    }
    else if (temp1 == "NaftasBarerls")
    {
        value = value/naftasBarerls;
    }



    if (temp2 == "Litri")
        value = value *litri;

    else if (temp2 == "Mililitri")
    {
        value = value*mililitri;
    }

    else if (temp2 == "Kubikmetrs")
    {
        value = value*kubikmetrs;
    }

    else if (temp2 == "Galons")
    {
         value = value*galons;
    }

    else if (temp2 == "Pintes")
    {
        value = value*pintes;
    }

    else if (temp2 == "NaftasBarerls")
    {
        value = value*naftasBarerls;
    }




   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->TilpumaOutput->setText(RealOutPut);
}

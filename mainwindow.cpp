#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "atrumaparvdialog.h"
#include "datuparvdialog.h"
#include "garumaparvdialog.h"
#include "laikaparvdialog.h"
#include "laukumaparvdialog.h"
#include "masasparvdialog.h"
#include "tilpumaparvdialog.h"
#include "datumaparvdialog.h"
#include "kalkulatoraspeledialog.h"
#include "valutasparvdialog.h"
#include "kalkulatorsdialog.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_AtrumaParvButton_clicked()
{
    AtrumaParvDialog AtrumaParv;
    AtrumaParv.setModal(true);
    AtrumaParv.exec();
}

void MainWindow::on_DatuParvButton_clicked()
{
    DatuParvDialog DatuParv;
    DatuParv.setModal(true);
    DatuParv.exec();
}


void MainWindow::on_GarumaParvButton_clicked()
{
    GarumaParvDialog GarumaParv;
    GarumaParv.setModal(true);
    GarumaParv.exec();
}

void MainWindow::on_TempParvButton_clicked()
{
    TempParvDialog TempParv;
    TempParv.setModal(true);
    TempParv.exec();
}

void MainWindow::on_LaukumaParvButton_clicked()
{
    LaukumaParvDialog LaukumaParv;
    LaukumaParv.setModal(true);
    LaukumaParv.exec();
}

void MainWindow::on_MasasParvButton_clicked()
{
    MasasParvDialog MasasParv;
    MasasParv.setModal(true);
    MasasParv.exec();
}

void MainWindow::on_TilpumaParvButton_clicked()
{
    TilpumaParvDialog TilpumaParv;
    TilpumaParv.setModal(true);
    TilpumaParv.exec();
}

void MainWindow::on_DatumaParvButton_clicked()
{
    DatumaParvDialog DatumaParv;
    DatumaParv.setModal(true);
    DatumaParv.exec();
}

void MainWindow::on_SpelesButton_clicked()
{
    KalkulatoraSpeleDialog KalkulatoraSpele;
    KalkulatoraSpele.setModal(true);
    KalkulatoraSpele.exec();
}

void MainWindow::on_ValutasParvButton_clicked()
{
    ValutasParvDialog ValutasParv;
    ValutasParv.setModal(true);
    ValutasParv.exec();
}

void MainWindow::on_StandartCalculatorButton_clicked()
{
    KalkulatorsDialog Kalkulators;
    Kalkulators.setModal(true);
    Kalkulators.exec();
}

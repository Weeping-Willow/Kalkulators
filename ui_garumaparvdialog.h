/********************************************************************************
** Form generated from reading UI file 'garumaparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GARUMAPARVDIALOG_H
#define UI_GARUMAPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GarumaParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *GarumaInput;
    QComboBox *comboBoxGarums1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *GarumsOutput;
    QComboBox *comboBoxGarums2;
    QPushButton *GarumsParvPushButton;

    void setupUi(QDialog *GarumaParvDialog)
    {
        if (GarumaParvDialog->objectName().isEmpty())
            GarumaParvDialog->setObjectName(QString::fromUtf8("GarumaParvDialog"));
        GarumaParvDialog->resize(400, 300);
        layoutWidget = new QWidget(GarumaParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 40, 331, 211));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        GarumaInput = new QDoubleSpinBox(layoutWidget);
        GarumaInput->setObjectName(QString::fromUtf8("GarumaInput"));
        GarumaInput->setDecimals(6);
        GarumaInput->setMaximum(9999999999.999998092651367);

        horizontalLayout_3->addWidget(GarumaInput);

        comboBoxGarums1 = new QComboBox(layoutWidget);
        comboBoxGarums1->setObjectName(QString::fromUtf8("comboBoxGarums1"));

        horizontalLayout_3->addWidget(comboBoxGarums1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        GarumsOutput = new QLineEdit(layoutWidget);
        GarumsOutput->setObjectName(QString::fromUtf8("GarumsOutput"));

        horizontalLayout_4->addWidget(GarumsOutput);

        comboBoxGarums2 = new QComboBox(layoutWidget);
        comboBoxGarums2->setObjectName(QString::fromUtf8("comboBoxGarums2"));

        horizontalLayout_4->addWidget(comboBoxGarums2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        GarumsParvPushButton = new QPushButton(layoutWidget);
        GarumsParvPushButton->setObjectName(QString::fromUtf8("GarumsParvPushButton"));

        verticalLayout->addWidget(GarumsParvPushButton);


        retranslateUi(GarumaParvDialog);

        QMetaObject::connectSlotsByName(GarumaParvDialog);
    } // setupUi

    void retranslateUi(QDialog *GarumaParvDialog)
    {
        GarumaParvDialog->setWindowTitle(QApplication::translate("GarumaParvDialog", "Dialog", nullptr));
        GarumsParvPushButton->setText(QApplication::translate("GarumaParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GarumaParvDialog: public Ui_GarumaParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GARUMAPARVDIALOG_H

/********************************************************************************
** Form generated from reading UI file 'laukumaparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LAUKUMAPARVDIALOG_H
#define UI_LAUKUMAPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LaukumaParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *LaukumaInput;
    QComboBox *comboBoxLaukuma1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *LaukumaOutput;
    QComboBox *comboBoxLaukuma2;
    QPushButton *LaukumaParvPushButton;

    void setupUi(QDialog *LaukumaParvDialog)
    {
        if (LaukumaParvDialog->objectName().isEmpty())
            LaukumaParvDialog->setObjectName(QString::fromUtf8("LaukumaParvDialog"));
        LaukumaParvDialog->resize(400, 300);
        layoutWidget = new QWidget(LaukumaParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(40, 40, 331, 211));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        LaukumaInput = new QDoubleSpinBox(layoutWidget);
        LaukumaInput->setObjectName(QString::fromUtf8("LaukumaInput"));
        LaukumaInput->setDecimals(6);
        LaukumaInput->setMaximum(9999999999.999998092651367);

        horizontalLayout_3->addWidget(LaukumaInput);

        comboBoxLaukuma1 = new QComboBox(layoutWidget);
        comboBoxLaukuma1->setObjectName(QString::fromUtf8("comboBoxLaukuma1"));

        horizontalLayout_3->addWidget(comboBoxLaukuma1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        LaukumaOutput = new QLineEdit(layoutWidget);
        LaukumaOutput->setObjectName(QString::fromUtf8("LaukumaOutput"));

        horizontalLayout_4->addWidget(LaukumaOutput);

        comboBoxLaukuma2 = new QComboBox(layoutWidget);
        comboBoxLaukuma2->setObjectName(QString::fromUtf8("comboBoxLaukuma2"));

        horizontalLayout_4->addWidget(comboBoxLaukuma2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        LaukumaParvPushButton = new QPushButton(layoutWidget);
        LaukumaParvPushButton->setObjectName(QString::fromUtf8("LaukumaParvPushButton"));

        verticalLayout->addWidget(LaukumaParvPushButton);


        retranslateUi(LaukumaParvDialog);

        QMetaObject::connectSlotsByName(LaukumaParvDialog);
    } // setupUi

    void retranslateUi(QDialog *LaukumaParvDialog)
    {
        LaukumaParvDialog->setWindowTitle(QApplication::translate("LaukumaParvDialog", "Dialog", nullptr));
        LaukumaParvPushButton->setText(QApplication::translate("LaukumaParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LaukumaParvDialog: public Ui_LaukumaParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LAUKUMAPARVDIALOG_H

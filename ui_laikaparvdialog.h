/********************************************************************************
** Form generated from reading UI file 'laikaparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LAIKAPARVDIALOG_H
#define UI_LAIKAPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TempParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *TempInput;
    QComboBox *comboBoxTemp1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *TempOutput;
    QComboBox *comboBoxTemp2;
    QPushButton *TempParvPushButton;

    void setupUi(QDialog *TempParvDialog)
    {
        if (TempParvDialog->objectName().isEmpty())
            TempParvDialog->setObjectName(QString::fromUtf8("TempParvDialog"));
        TempParvDialog->resize(400, 300);
        layoutWidget = new QWidget(TempParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 40, 331, 211));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        TempInput = new QDoubleSpinBox(layoutWidget);
        TempInput->setObjectName(QString::fromUtf8("TempInput"));
        TempInput->setDecimals(0);
        TempInput->setMinimum(-1000000.000000000000000);
        TempInput->setMaximum(99999999999.000000000000000);

        horizontalLayout_3->addWidget(TempInput);

        comboBoxTemp1 = new QComboBox(layoutWidget);
        comboBoxTemp1->setObjectName(QString::fromUtf8("comboBoxTemp1"));

        horizontalLayout_3->addWidget(comboBoxTemp1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        TempOutput = new QLineEdit(layoutWidget);
        TempOutput->setObjectName(QString::fromUtf8("TempOutput"));

        horizontalLayout_4->addWidget(TempOutput);

        comboBoxTemp2 = new QComboBox(layoutWidget);
        comboBoxTemp2->setObjectName(QString::fromUtf8("comboBoxTemp2"));

        horizontalLayout_4->addWidget(comboBoxTemp2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        TempParvPushButton = new QPushButton(layoutWidget);
        TempParvPushButton->setObjectName(QString::fromUtf8("TempParvPushButton"));

        verticalLayout->addWidget(TempParvPushButton);


        retranslateUi(TempParvDialog);

        QMetaObject::connectSlotsByName(TempParvDialog);
    } // setupUi

    void retranslateUi(QDialog *TempParvDialog)
    {
        TempParvDialog->setWindowTitle(QApplication::translate("TempParvDialog", "Dialog", nullptr));
        TempParvPushButton->setText(QApplication::translate("TempParvDialog", "Parveidot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TempParvDialog: public Ui_TempParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LAIKAPARVDIALOG_H

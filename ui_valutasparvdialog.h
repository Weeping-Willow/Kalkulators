/********************************************************************************
** Form generated from reading UI file 'valutasparvdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VALUTASPARVDIALOG_H
#define UI_VALUTASPARVDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ValutasParvDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *valutasInput;
    QComboBox *comboBoxValutas1;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *valutaOutput;
    QComboBox *comboBoxValutas2;
    QPushButton *TilpumaParvPushButton;
    QLabel *label;
    QLabel *dateOutput;

    void setupUi(QDialog *ValutasParvDialog)
    {
        if (ValutasParvDialog->objectName().isEmpty())
            ValutasParvDialog->setObjectName(QString::fromUtf8("ValutasParvDialog"));
        ValutasParvDialog->resize(589, 351);
        layoutWidget = new QWidget(ValutasParvDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 60, 541, 271));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        valutasInput = new QDoubleSpinBox(layoutWidget);
        valutasInput->setObjectName(QString::fromUtf8("valutasInput"));
        valutasInput->setDecimals(2);
        valutasInput->setMaximum(10000000000.000000000000000);

        horizontalLayout_3->addWidget(valutasInput);

        comboBoxValutas1 = new QComboBox(layoutWidget);
        comboBoxValutas1->setObjectName(QString::fromUtf8("comboBoxValutas1"));

        horizontalLayout_3->addWidget(comboBoxValutas1);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        valutaOutput = new QLineEdit(layoutWidget);
        valutaOutput->setObjectName(QString::fromUtf8("valutaOutput"));

        horizontalLayout_4->addWidget(valutaOutput);

        comboBoxValutas2 = new QComboBox(layoutWidget);
        comboBoxValutas2->setObjectName(QString::fromUtf8("comboBoxValutas2"));

        horizontalLayout_4->addWidget(comboBoxValutas2);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        TilpumaParvPushButton = new QPushButton(layoutWidget);
        TilpumaParvPushButton->setObjectName(QString::fromUtf8("TilpumaParvPushButton"));

        verticalLayout->addWidget(TilpumaParvPushButton);

        label = new QLabel(ValutasParvDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 20, 351, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans Adlam"));
        font.setPointSize(14);
        label->setFont(font);
        dateOutput = new QLabel(ValutasParvDialog);
        dateOutput->setObjectName(QString::fromUtf8("dateOutput"));
        dateOutput->setGeometry(QRect(250, 20, 331, 31));
        dateOutput->setFont(font);

        retranslateUi(ValutasParvDialog);

        QMetaObject::connectSlotsByName(ValutasParvDialog);
    } // setupUi

    void retranslateUi(QDialog *ValutasParvDialog)
    {
        ValutasParvDialog->setWindowTitle(QApplication::translate("ValutasParvDialog", "Dialog", nullptr));
        TilpumaParvPushButton->setText(QApplication::translate("ValutasParvDialog", "Parveidot", nullptr));
        label->setText(QApplication::translate("ValutasParvDialog", "Atjaunin\304\201\305\241anas datums:", nullptr));
        dateOutput->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ValutasParvDialog: public Ui_ValutasParvDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VALUTASPARVDIALOG_H

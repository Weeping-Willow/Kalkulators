#ifndef LAIKAPARVDIALOG_H
#define LAIKAPARVDIALOG_H

#include <QDialog>

namespace Ui {
class TempParvDialog;
}

class TempParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TempParvDialog(QWidget *parent = nullptr);
    ~TempParvDialog();

private slots:
    void on_TempParvPushButton_clicked();

private:
    Ui::TempParvDialog *ui;
};

#endif // LAIKAPARVDIALOG_H

#include "valutasparvdialog.h"
#include "ui_valutasparvdialog.h"
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <string>
#include <cmath>
#include <QDebug>
#include <QtSql>
#include <QFileInfo>
#include <QMessageBox>
#include <vector>

float currencyUSD =22 ;
float currencyEUR = 1;
float currencyGBP = 10;
float currencyRUB = 11;
float currencyCNY = 55;
float currencyINR = 23;


ValutasParvDialog::ValutasParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ValutasParvDialog)
{
    ui->setupUi(this);

    QSqlDatabase dbCurrencyValues=QSqlDatabase::addDatabase("QSQLITE");
    dbCurrencyValues.setDatabaseName("valuesCurrency.db");
    qDebug()<<dbCurrencyValues.open();
    if (!dbCurrencyValues.open())
    {
        QMessageBox::warning(this,"Nav datubaze","Nav pieejama datubaze");
    }

    QStringList Names= {"EUR","USD","GBP","RUB","CNY","INR"};
    foreach (QString item, Names)
    {
        ui->comboBoxValutas1->addItem(item);
        ui->comboBoxValutas2->addItem(item);
    }

    //pievieno comboBox šos nosaukumus

    QNetworkAccessManager * manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(fileIsReady(QNetworkReply*)) );

    //kad url request ir pabeigts tad ieslegt funkciju fileIsReady
    manager->get(QNetworkRequest(QUrl("https://api.exchangeratesapi.io/latest")));

}
//sūta pieprasījumu uz mājaslapu
//https://api.exchangeratesapi.io/latest
//https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml?1a36451c675665ee2472fbcdb9ece4fd
//valutas maiņas vērtības nolādējamās mājaslapas
ValutasParvDialog::~ValutasParvDialog()
{
    delete ui;
}



void ValutasParvDialog::fileIsReady( QNetworkReply * reply)
{
    QSqlQuery qry;

    QString DataAsString = QString::fromStdString(reply->readAll().toStdString());


    std::string jsonExchangeRates = DataAsString.toStdString();

//Parveido Bytearray uz QString un no QString uz std::string

    if (DataAsString.isEmpty()){
    //ja nav pieejams internets šīs if ieslegsies ta vieta
        QMessageBox::warning(this,"Internets","Nav pieejami dati");
        
        qry.prepare("SELECT ValueCurrency FROM Currency WHERE ID=1; "); //sagatavo querry priekš datubāzes
        qry.exec(); //izpilda so querry
        qry.first(); // pirmais loceklis kurs tika ieguts
        QString temp = qry.value(0).toString(); // saglaba so locekli temp variabla
        currencyCNY  =std::stof(temp.toStdString()); //parveido uz float


        qry.prepare("SELECT ValueCurrency FROM Currency WHERE ID=2; ");
        qry.exec();
        qry.first();
        temp = qry.value(0).toString();
        currencyGBP=std::stof(temp.toStdString());

        qry.prepare("SELECT ValueCurrency FROM Currency WHERE ID=3; ");
        qry.exec();
        qry.first();
        temp = qry.value(0).toString();
        currencyINR=std::stof(temp.toStdString());


        qry.prepare("SELECT ValueCurrency FROM Currency WHERE ID=4; ");
        qry.exec();
        qry.first();
        temp = qry.value(0).toString();
        currencyRUB=std::stof(temp.toStdString());


        qry.prepare("SELECT ValueCurrency FROM Currency WHERE ID=5; ");
        qry.exec();
        qry.first();
        temp = qry.value(0).toString();
        currencyUSD=std::stof(temp.toStdString());


        qry.prepare("SELECT UpdateDate FROM Date WHERE ID=1; ");
        qry.exec();
        qry.first();
        temp = qry.value(0).toString()+" Dati ir veci";
        qDebug ()<<temp;
        ui->dateOutput->setText(temp);

        return;

    }
    int  valueRUBRange1 = jsonExchangeRates.find("RUB")+5;
    int  valueRUBRange2 = jsonExchangeRates.find("CAD")-2;


//atrod vajadzīgās vērtības sākum un beigu punktu
    std::string number1 = jsonExchangeRates.substr(valueRUBRange1,valueRUBRange2-valueRUBRange1);

//izņem no lielākā string mazu stringu ar vajadzīgo vērtību

    qDebug()<<3;
    valueRUBRange1 = number1.find(".")+1;
    valueRUBRange2 = jsonExchangeRates.find("CAD")-2;

    std::string number2 = number1.substr(valueRUBRange1,2);

//iegūt decimāl skaitli no visa skaitli
    currencyRUB = std::stof(number1); //+ std::stof(number2)/100;//nonemt šīs svītras ja kompile priekš linux sistēmas
//atjaunot valūtas vērtību




    int  valueUSDRange1 = jsonExchangeRates.find("USD")+5;
    int  valueUSDRange2 = jsonExchangeRates.find("PHP")-2;


    number1 = jsonExchangeRates.substr(valueUSDRange1,valueUSDRange2-valueUSDRange1);

    valueUSDRange1 = number1.find(".")+1;
    valueUSDRange2 = jsonExchangeRates.find("PHP")-2;

    number2 = number1.substr(valueUSDRange1,2);

    currencyUSD = std::stof(number1) ;//+ std::stof(number2)/pow(10,number2.length());//nonemt šīs svītras ja kompile priekš linux sistēmas





    int  valueGBPRange1 = jsonExchangeRates.find("GBP")+5;
    int  valueGBPRange2 = jsonExchangeRates.find("MXN")-2;


    number1 = jsonExchangeRates.substr(valueGBPRange1,valueGBPRange2-valueGBPRange1);

    valueGBPRange1 = number1.find(".")+1;
    valueGBPRange2 = jsonExchangeRates.find("MXN")-2;

    number2 = number1.substr(valueGBPRange1,2);

    currencyGBP = std::stof(number1) ;//+ std::stof(number2)/pow(10,number2.length());//nonemt šīs svītras ja kompile priekš linux sistēmas




    int  valueCNYRange1 = jsonExchangeRates.find("CNY")+5;
    int  valueCNYRange2 = jsonExchangeRates.find("SEK")-2;


    number1 = jsonExchangeRates.substr(valueCNYRange1,valueCNYRange2-valueCNYRange1);

    valueCNYRange1 = number1.find(".")+1;
    valueCNYRange2 = jsonExchangeRates.find("SEK")-2;

    number2 = number1.substr(valueCNYRange1,2);


    currencyCNY = std::stof(number1);// + std::stof(number2)/pow(10,number2.length());//nonemt šīs svītras ja kompile priekš linux sistēmas




    int  valueINRRange1 = jsonExchangeRates.find("INR")+5;
    int  valueINRRange2 = jsonExchangeRates.find("RON")-2;


    number1 = jsonExchangeRates.substr(valueINRRange1,valueINRRange2-valueINRRange1);

    valueINRRange1 = number1.find(".")+1;
    valueINRRange2 = jsonExchangeRates.find("RON")-2;

    number2 = number1.substr(valueINRRange1,2);

    currencyINR = std::stof(number1) ;//+ std::stof(number2)/pow(10,number2.length());//nonemt šīs svītras ja kompile priekš linux sistēmas



    int valueDateRange1 = jsonExchangeRates.find("date")+7;
    std::string date = jsonExchangeRates.substr(valueDateRange1,10);
    ui->dateOutput->setText(QString::fromStdString(date));

    //iegūt datumu ,kad valūta atjaunota un tad to parādīt lietotājam


    std::vector<float> currencyValuesInVector= {currencyCNY,currencyGBP,currencyINR,currencyRUB,currencyUSD};


    int it = 0;

    for (auto i =currencyValuesInVector.begin();i!= currencyValuesInVector.end();++i)
    {
        it++;
        QString temp1 = QString::fromStdString(std::to_string(*i));
        QString temp2 = QString::fromStdString(std::to_string(it));
        qry.prepare("UPDATE Currency SET ValueCurrency ='"+temp1+"' WHERE ID='"+temp2+"'; ");

        qDebug()<<qry.executedQuery();
       if(qry.exec())
        {
            qDebug()<<"nice";
        }
        else
        {
           QMessageBox::warning(this,"Datubaze","Kļūda strādājot ar datubāzi 1");
        }
    }

    qry.prepare("UPDATE Date SET UpdateDate ='"+QString::fromStdString(date)+"' WHERE ID=1; ");
    if(qry.exec())
     {
         qDebug()<<"nice";
     }
     else
     {
        QMessageBox::warning(this,"Datubaze","Kļūda strādājot ar datubāzi 2");
     }

}

void ValutasParvDialog::on_TilpumaParvPushButton_clicked()
{

    float value;
    QString temp1,temp2;

    temp1 =ui->comboBoxValutas1->currentText();
    temp2 =ui->comboBoxValutas2->currentText();
    value = ui->valutasInput->value();

    if (temp1 == "EUR")
        value = value /currencyEUR;

    else if (temp1 == "USD")
    {
        value = value/currencyUSD;
    }

    else if (temp1 == "GBP")
    {
        value = value/currencyGBP;
    }

    else if (temp1 == "INR")
    {
         value = value/currencyINR;
    }

    else if (temp1 == "CNY")
    {
        value = value/currencyCNY;
    }
    else if (temp1 == "RUB")
    {
        value = value/currencyRUB;
    }



    if (temp2 == "EUR")
        value = value *currencyEUR;

    else if (temp2 == "USD")
    {
        value = value*currencyUSD;
    }

    else if (temp2 == "GBP")
    {
        value = value*currencyGBP;
    }

    else if (temp2 == "INR")
    {
         value = value*currencyINR;
    }

    else if (temp2 == "CNY")
    {
        value = value*currencyCNY;
    }

    else if (temp2 == "RUB")
    {
        value = value*currencyRUB;
    }




   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->valutaOutput->setText(RealOutPut);
}

//iegūt valūtu no kuras lietotājs grib pārveidot un tad iegūt valūtu uz kuru lietotājs grib pārveidot tad iegūst vērtību ,ko grib pārveidot šo vērtību dalīt ar viņa izvēlētā 1.kursa vērtību ,kura atbilst galvanās
//vērtības 1 vienībai un tad so pašu vērtību reizināt ar otras naudas kursa vērtībuj,kura atbilst vienai vērtībai no galvanā vienības.

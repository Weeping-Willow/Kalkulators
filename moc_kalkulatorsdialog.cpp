/****************************************************************************
** Meta object code from reading C++ file 'kalkulatorsdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "kalkulatorsdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kalkulatorsdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KalkulatorsDialog_t {
    QByteArrayData data[15];
    char stringdata0[308];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KalkulatorsDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KalkulatorsDialog_t qt_meta_stringdata_KalkulatorsDialog = {
    {
QT_MOC_LITERAL(0, 0, 17), // "KalkulatorsDialog"
QT_MOC_LITERAL(1, 18, 12), // "digitPressed"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 21), // "on_buttonDot_released"
QT_MOC_LITERAL(4, 54, 29), // "on_buttonNevienadiba_released"
QT_MOC_LITERAL(5, 84, 23), // "on_buttonClear_released"
QT_MOC_LITERAL(6, 108, 26), // "on_buttonProcents_released"
QT_MOC_LITERAL(7, 135, 22), // "on_buttonDala_released"
QT_MOC_LITERAL(8, 158, 20), // "on_buttonPi_released"
QT_MOC_LITERAL(9, 179, 16), // "vienkVienadojumi"
QT_MOC_LITERAL(10, 196, 23), // "on_buttonEqual_released"
QT_MOC_LITERAL(11, 220, 22), // "on_buttonRoot_released"
QT_MOC_LITERAL(12, 243, 21), // "on_buttonSin_released"
QT_MOC_LITERAL(13, 265, 21), // "on_buttonCos_released"
QT_MOC_LITERAL(14, 287, 20) // "on_buttonTg_released"

    },
    "KalkulatorsDialog\0digitPressed\0\0"
    "on_buttonDot_released\0"
    "on_buttonNevienadiba_released\0"
    "on_buttonClear_released\0"
    "on_buttonProcents_released\0"
    "on_buttonDala_released\0on_buttonPi_released\0"
    "vienkVienadojumi\0on_buttonEqual_released\0"
    "on_buttonRoot_released\0on_buttonSin_released\0"
    "on_buttonCos_released\0on_buttonTg_released"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KalkulatorsDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x08 /* Private */,
       3,    0,   80,    2, 0x08 /* Private */,
       4,    0,   81,    2, 0x08 /* Private */,
       5,    0,   82,    2, 0x08 /* Private */,
       6,    0,   83,    2, 0x08 /* Private */,
       7,    0,   84,    2, 0x08 /* Private */,
       8,    0,   85,    2, 0x08 /* Private */,
       9,    0,   86,    2, 0x08 /* Private */,
      10,    0,   87,    2, 0x08 /* Private */,
      11,    0,   88,    2, 0x08 /* Private */,
      12,    0,   89,    2, 0x08 /* Private */,
      13,    0,   90,    2, 0x08 /* Private */,
      14,    0,   91,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void KalkulatorsDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<KalkulatorsDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->digitPressed(); break;
        case 1: _t->on_buttonDot_released(); break;
        case 2: _t->on_buttonNevienadiba_released(); break;
        case 3: _t->on_buttonClear_released(); break;
        case 4: _t->on_buttonProcents_released(); break;
        case 5: _t->on_buttonDala_released(); break;
        case 6: _t->on_buttonPi_released(); break;
        case 7: _t->vienkVienadojumi(); break;
        case 8: _t->on_buttonEqual_released(); break;
        case 9: _t->on_buttonRoot_released(); break;
        case 10: _t->on_buttonSin_released(); break;
        case 11: _t->on_buttonCos_released(); break;
        case 12: _t->on_buttonTg_released(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject KalkulatorsDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_KalkulatorsDialog.data,
    qt_meta_data_KalkulatorsDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *KalkulatorsDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KalkulatorsDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KalkulatorsDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int KalkulatorsDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

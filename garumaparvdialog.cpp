#include "garumaparvdialog.h"
#include "ui_garumaparvdialog.h"

GarumaParvDialog::GarumaParvDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GarumaParvDialog)
{
    ui->setupUi(this);
    QStringList Names= {"Metri","Jūdzas","Kilometri","Pēdas","Centimetri"};
    foreach (QString item, Names)
    {
        ui->comboBoxGarums1->addItem(item);
        ui->comboBoxGarums2->addItem(item);
    }
    //pievieno comboBox šos nosaukumus
}

GarumaParvDialog::~GarumaParvDialog()
{
    delete ui;
}

void GarumaParvDialog::on_GarumsParvPushButton_clicked()
{
    const double metri =1;
    const double judzas = 0.00062137119223733 ;
    const double kilometri =0.001;
    const double pedas =3.2808398950131;
    const double centimetri =100;


    double value;
    QString temp1,temp2;

    temp1 =ui->comboBoxGarums1->currentText();
    temp2 =ui->comboBoxGarums2->currentText();
    value = ui->GarumaInput->value();
    if (temp1 == "Metri")
        value = value /metri;

    else if (temp1 == "Jūdzas")
    {
        value = value/judzas;
    }

    else if (temp1 == "Kilometri")
    {
        value = value/kilometri;
    }

    else if (temp1 == "Pēdas")
    {
         value = value/pedas;
    }

    else if (temp1 == "Centimetri")
    {
        value = value/centimetri;
    }



    if (temp2 == "Metri")
        value = value *metri;

    else if (temp2 == "Jūdzas")
    {
        value = value*judzas;
    }

    else if (temp2 == "Kilometri")
    {
        value = value*kilometri;
    }

    else if (temp2 == "Pēdas")
    {
         value = value*pedas;
    }

    else if (temp2 == "Centimetri")
    {
        value = value*centimetri;
    }



   std::string OutPut = std::to_string(value);
   QString RealOutPut = QString::fromStdString(OutPut);
   ui->GarumsOutput->setText(RealOutPut);
}

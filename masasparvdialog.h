#ifndef MASASPARVDIALOG_H
#define MASASPARVDIALOG_H

#include <QDialog>

namespace Ui {
class MasasParvDialog;
}

class MasasParvDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MasasParvDialog(QWidget *parent = nullptr);
    ~MasasParvDialog();

private slots:
    void on_MasasParvPushButton_clicked();

private:
    Ui::MasasParvDialog *ui;
};

#endif // MASASPARVDIALOG_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_AtrumaParvButton_clicked();

    void on_DatuParvButton_clicked();

    void on_GarumaParvButton_clicked();

    void on_TempParvButton_clicked();

    void on_LaukumaParvButton_clicked();

    void on_MasasParvButton_clicked();

    void on_TilpumaParvButton_clicked();

    void on_DatumaParvButton_clicked();

    void on_SpelesButton_clicked();

    void on_ValutasParvButton_clicked();

    void on_StandartCalculatorButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

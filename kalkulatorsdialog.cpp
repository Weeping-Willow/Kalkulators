#include "kalkulatorsdialog.h"
#include "ui_kalkulatorsdialog.h"
#include <cmath>


const double pi =3.141592653589793;
double labelNumber1;
bool userTypingSecondNumber =false ;

KalkulatorsDialog::KalkulatorsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KalkulatorsDialog)
{
    ui->setupUi(this);
    connect(ui->button0,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button1,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button2,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button3,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button4,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button5,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button6,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button7,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button8,SIGNAL(released()),this,SLOT(digitPressed()));
    connect(ui->button9,SIGNAL(released()),this,SLOT(digitPressed()));

    //Kad poga ir atlais tiek ieslēgta funkcija digitPressed


    connect(ui->buttonPlus,SIGNAL(released()),this,SLOT(vienkVienadojumi()));
    connect(ui->buttonMinus,SIGNAL(released()),this,SLOT(vienkVienadojumi()));
    connect(ui->buttonReiz,SIGNAL(released()),this,SLOT(vienkVienadojumi()));
    connect(ui->buttonDivide,SIGNAL(released()),this,SLOT(vienkVienadojumi()));
    connect(ui->buttonKapinasana,SIGNAL(released()),this,SLOT(vienkVienadojumi()));

    //Kad poga ir atlais tiek ieslēgta funkcija vienkVienadojumi

    ui->buttonPlus->setCheckable(true);
    ui->buttonMinus->setCheckable(true);
    ui->buttonReiz->setCheckable(true);
    ui->buttonDivide->setCheckable(true);
    ui->buttonKapinasana->setCheckable(true);

    //pogam tiek piešķirts bool vertiba,kas defaulta ir false;

}

KalkulatorsDialog::~KalkulatorsDialog()
{
    delete ui;
}


void KalkulatorsDialog::digitPressed()
{
    QPushButton * button = (QPushButton*)sender();
    //saglaba pointer uz pogu ,kas tika nospiesta
    double labelNumber;
    QString labelNumberOutput;


    if ((ui->buttonPlus->isChecked()||ui->buttonMinus->isChecked()||ui->buttonReiz->isChecked()||ui->buttonDivide->isChecked()||ui->buttonKapinasana->isChecked())&&(!userTypingSecondNumber))
    {
        labelNumber = button->text().toDouble();
        userTypingSecondNumber = true;
        labelNumberOutput =QString::number(labelNumber,'g',15);
    }
    //if parabauda vai šis ir otrais cipars un vai lietotajs neraksta decimal skaitli
    //planotas izvades vertiba ir vienada ar nospiesto pogu un lietotajs raksta otru ciparu ir uzlikts uz true un tad to izvada uz lietotaja ekrana
    else
    {
        if (ui->output->text().contains('.') && button->text()== "0")
        {
          labelNumberOutput = ui->output->text()+button->text();
          //pievieno uzpiesto ciparu skaitlim
        }
        else
        {
            labelNumber =(ui->output->text()+button->text()).toDouble();
            labelNumberOutput =QString::number(labelNumber,'g',15);
            //pievieno uzpiesto ciparu skaitlim
        }
    }
    ui->output->setText(labelNumberOutput);
    //izvada lietotajam
}


void KalkulatorsDialog::on_buttonDot_released()
{
    QString temp1 = ui->output->text();
    std::string temp2 = temp1.toStdString();
    std::size_t found;

    found = temp2.find('.');

    if (found!=std::string::npos);

     else
    {
    ui->output->setText(ui->output->text()+ ".");
    }
//Parbauda vai ir punkts
}






void KalkulatorsDialog::on_buttonNevienadiba_released()
{
    double labelNumber = ui ->output->text().toDouble();
    if (labelNumber == 0);

    else
    {
        labelNumber = labelNumber * -1;
        QString labelNumberOutput = QString::number(labelNumber,'g',15);
        ui->output->setText(labelNumberOutput);
    }
}



void KalkulatorsDialog::on_buttonClear_released()
{
    ui->buttonPlus ->setChecked(false);
    ui->buttonMinus ->setChecked(false);
    ui->buttonReiz ->setChecked(false);
    ui->buttonDivide ->setChecked(false);
    ui->buttonKapinasana ->setChecked(false);

    userTypingSecondNumber = false;

    ui->output->setText("0");
}

void KalkulatorsDialog::on_buttonProcents_released()
{
    double labelNumber = ui ->output->text().toDouble();
    labelNumber = labelNumber * 0.01;
    QString labelNumberOutput = QString::number(labelNumber,'g',15);
    ui->output->setText(labelNumberOutput);
}

void KalkulatorsDialog::on_buttonDala_released()
{
    double labelNumber = ui ->output->text().toDouble();
    labelNumber = 1 /labelNumber ;
    QString labelNumberOutput = QString::number(labelNumber,'g',13);
    ui->output->setText(labelNumberOutput);
}

void KalkulatorsDialog::on_buttonPi_released()
{
    double labelNumber = pi;
    QString labelNumberOutput = QString::number(labelNumber,'g',15);
    ui->output->setText(labelNumberOutput);
}

void KalkulatorsDialog::vienkVienadojumi()
{
     QPushButton * button = (QPushButton*)sender();


     labelNumber1= ui->output->text().toDouble();

     button->setChecked(true);


}

//kad jebkura no pogam ir nospiesta tad vina ir atzimeta ka true
void KalkulatorsDialog::on_buttonEqual_released()
{
    double labelNumber,labelNumber2;
    labelNumber2= ui->output->text().toDouble();

    QString LabelNumberOutput;

    if (ui->buttonPlus->isChecked())
    {
        labelNumber = labelNumber1+labelNumber2;
        LabelNumberOutput = QString::number(labelNumber,'g',15);
        ui->output->setText(LabelNumberOutput);
        ui->buttonPlus->setChecked(false);
    }

    else if (ui->buttonMinus->isChecked())
    {
        labelNumber = labelNumber1-labelNumber2;
        LabelNumberOutput = QString::number(labelNumber,'g',15);
        ui->output->setText(LabelNumberOutput);
        ui->buttonMinus->setChecked(false);
    }

    else if (ui->buttonReiz->isChecked())
    {
        labelNumber = labelNumber1*labelNumber2;
        LabelNumberOutput = QString::number(labelNumber,'g',15);
        ui->output->setText(LabelNumberOutput);
        ui->buttonReiz->setChecked(false);
    }

    else if (ui->buttonDivide->isChecked())
    {
        labelNumber = labelNumber1/labelNumber2;
        LabelNumberOutput = QString::number(labelNumber,'g',15);
        ui->output->setText(LabelNumberOutput);
        ui->buttonDivide->setChecked(false);
    }
    else if (ui->buttonKapinasana->isChecked())
    {
        labelNumber = pow(labelNumber1,labelNumber2);
        LabelNumberOutput = QString::number(labelNumber,'g',15);
        ui->output->setText(LabelNumberOutput);
        ui->buttonDivide->setChecked(false);
    }
    userTypingSecondNumber = false;
}

void KalkulatorsDialog::on_buttonRoot_released()
{
    double labelNumber = ui->output->text().toDouble();
    labelNumber = pow(labelNumber,0.5);
    QString LabelNumberOutput = QString::number(labelNumber,'g',15);
    ui->output->setText(LabelNumberOutput);
}

void KalkulatorsDialog::on_buttonSin_released()
{
    double labelNumber = ui->output->text().toDouble();
    labelNumber = sin(labelNumber);
    QString LabelNumberOutput = QString::number(labelNumber,'g',15);
    ui->output->setText(LabelNumberOutput);
}

void KalkulatorsDialog::on_buttonCos_released()
{
    double labelNumber = ui->output->text().toDouble();
    labelNumber = cos(labelNumber);
    QString LabelNumberOutput = QString::number(labelNumber,'g',15);
    ui->output->setText(LabelNumberOutput);
}

void KalkulatorsDialog::on_buttonTg_released()
{
    double labelNumber = ui->output->text().toDouble();
    labelNumber = tan(labelNumber);
    QString LabelNumberOutput = QString::number(labelNumber,'g',15);
    ui->output->setText(LabelNumberOutput);
}
